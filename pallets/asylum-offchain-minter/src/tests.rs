use crate::{mock::*, Error, ItemMintRequest, MintRequests};
use codec::Decode;
use frame_support::{
	assert_noop, assert_ok,
	pallet_prelude::Get,
	traits::{Currency, Hooks},
	BoundedVec,
};
use sp_core::offchain::{testing, OffchainWorkerExt, TransactionPoolExt};
use sp_keystore::{testing::KeyStore, KeystoreExt, SyncCryptoStore};
use sp_runtime::{app_crypto::Pair, RuntimeAppPublic};
use sp_std::collections::btree_set::BTreeSet;
use std::sync::Arc;

const PHRASE: &str = "news slush supreme milk chapter athlete soap sausage put clutch what kitten";

fn bounded<T>(string: &str) -> BoundedVec<u8, T>
where
	T: Get<u32>,
{
	TryInto::<BoundedVec<u8, T>>::try_into(string.as_bytes().to_vec()).unwrap()
}

pub fn test_account(byte: u8) -> sp_core::sr25519::Public {
	sp_core::sr25519::Public::from_raw([byte; 32])
}

pub fn setup() {
	let signer = sp_core::sr25519::Pair::from_phrase(PHRASE, None).unwrap().0.public();
	Balances::make_free_balance_be(&signer, 10 << 10);
	assert_ok!(Spaces::create_space(Origin::signed(signer), 0, vec![test_account(1)], Some(0),));
	assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(signer), 0, true));
	assert_ok!(Spaces::mint_space_pass(Origin::signed(test_account(2)), 0, 0, test_account(2)));
	assert_ok!(Spaces::mint_space_pass(Origin::signed(test_account(3)), 0, 1, test_account(3)));
	assert_ok!(Blueprints::create_blueprint(
		Origin::signed(signer),
		bounded("MyBlueprint"),
		bounded("Hash"),
		None,
		vec![].try_into().unwrap()
	));
	assert_ok!(Spaces::add_blueprint_support(Origin::signed(signer), 0, 0));

	// Set servers
	let server_a = test_account(42);
	let servers = BTreeSet::from([server_a]);
	assert_ok!(Minter::set_space_servers(Origin::root(), 0, servers));
}

#[test]
fn set_servers_should_work() {
	ExtBuilder::default().build().execute_with(|| {
		assert_noop!(
			Minter::set_space_servers(Origin::root(), 0, Default::default()),
			Error::<Test>::SpaceDoesntExist
		);
		assert_ok!(Spaces::create_space(
			Origin::signed(test_account(1)),
			0,
			vec![test_account(1)],
			None,
		));
		let servers = BTreeSet::from([test_account(1), test_account(2)]);
		assert_ok!(Minter::set_space_servers(Origin::root(), 0, servers));
		let servers = BTreeSet::from([test_account(1), test_account(2), test_account(3)]);
		assert_noop!(
			Minter::set_space_servers(Origin::root(), 0, servers),
			Error::<Test>::ServerLimitExceeded
		);
	});
}

#[test]
fn request_mint_should_work() {
	ExtBuilder::default().build().execute_with(|| {
		let origin = test_account(1);
		let recipient = test_account(2);
		// Space doesn't exist, so we don't have servers associated with this space
		assert_noop!(
			Minter::request_item_mint(Origin::signed(origin), recipient, 0, 0, bounded("")),
			Error::<Test>::NoAccessToMinting
		);

		assert_ok!(Spaces::create_space(Origin::signed(origin), 0, vec![origin], Some(0)));
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(origin), 0, true));
		// Space exists, but we still don't have associated servers
		assert_noop!(
			Minter::request_item_mint(Origin::signed(origin), recipient, 0, 0, bounded("")),
			Error::<Test>::NoAccessToMinting
		);

		// Set servers
		let server_a = test_account(42);
		let server_b = test_account(69);
		let servers = BTreeSet::from([server_a, server_b]);
		assert_ok!(Minter::set_space_servers(Origin::root(), 0, servers));

		// We have space, servers, but account doesn't own space pass
		assert_noop!(
			Minter::request_item_mint(Origin::signed(server_a), recipient, 0, 0, bounded("")),
			Error::<Test>::NoAccessToSpace
		);

		// Account mints space
		assert_ok!(Spaces::mint_space_pass(Origin::signed(recipient), 0, 0, recipient));

		// We have space, servers, account has space space pass, but space doesn't support blueprint
		// with id 0 (doesn't exists at all)
		assert_noop!(
			Minter::request_item_mint(Origin::signed(server_a), recipient, 0, 0, bounded("")),
			Error::<Test>::SpaceDoesntSupportBlueprint
		);

		// Create blueprint
		assert_ok!(Blueprints::create_blueprint(
			Origin::signed(origin),
			bounded("MyBlueprint"),
			bounded("Hash"),
			None,
			vec![].try_into().unwrap()
		));
		assert_ok!(Spaces::add_blueprint_support(Origin::signed(origin), 0, 0));

		// Finally, succesfull mint request
		assert_ok!(Minter::request_item_mint(
			Origin::signed(server_a),
			recipient,
			0,
			0,
			bounded("")
		));
		assert_ok!(Minter::request_item_mint(
			Origin::signed(server_b),
			recipient,
			0,
			0,
			bounded("")
		));
		assert_eq!(
			Minter::mint_requests(server_a).unwrap(),
			vec![ItemMintRequest {
				item_recipient: recipient,
				blueprint_id: 0,
				item_metadata: bounded("")
			}]
		);
		assert_eq!(
			Minter::mint_requests(server_b).unwrap(),
			vec![ItemMintRequest {
				item_recipient: recipient,
				blueprint_id: 0,
				item_metadata: bounded("")
			}]
		);
	});
}

#[test]
fn should_mint_item() {
	let (offchain, _offchain_state) = testing::TestOffchainExt::new();
	let (pool, pool_state) = testing::TestTransactionPoolExt::new();
	let keystore = KeyStore::new();
	SyncCryptoStore::sr25519_generate_new(
		&keystore,
		crate::sr25519::app_sr25519::Public::ID,
		Some(&format!("{}///", PHRASE)),
	)
	.unwrap();

	let mut t = ExtBuilder::default().build();
	t.register_extension(OffchainWorkerExt::new(offchain));
	t.register_extension(TransactionPoolExt::new(pool));
	t.register_extension(KeystoreExt(Arc::new(keystore)));

	t.execute_with(|| {
		setup();
		let server_a = test_account(42);
		let recipient_0 = test_account(2);
		let recipient_1 = test_account(3);

		// Test single mint request
		assert_ok!(Minter::request_item_mint(
			Origin::signed(server_a),
			recipient_0,
			0,
			0,
			bounded("")
		));
		Minter::offchain_worker(1);
		// then
		let tx = pool_state.write().transactions.pop().unwrap();
		let tx = Extrinsic::decode(&mut &*tx).unwrap();
		assert_eq!(tx.signature.unwrap().0, 0);
		assert_eq!(
			tx.call,
			Call::Blueprints(pallet_asylum_blueprints::Call::<Test>::mint_item {
				owner: recipient_0,
				blueprint_id: 0,
				metadata: bounded("")
			})
		);
		assert!(pool_state.read().transactions.is_empty());
		assert!(MintRequests::<Test>::iter_values().collect::<Vec<_>>().is_empty());

		// Test several mint request
		assert_ok!(Minter::request_item_mint(
			Origin::signed(server_a),
			recipient_0,
			0,
			0,
			bounded("")
		));
		assert_ok!(Minter::request_item_mint(
			Origin::signed(server_a),
			recipient_1,
			0,
			0,
			bounded("")
		));
		assert_eq!(Minter::mint_requests(server_a).unwrap().len(), 2);
		Minter::offchain_worker(2);
		let tx = pool_state.write().transactions.pop().unwrap();
		let tx = Extrinsic::decode(&mut &*tx).unwrap();
		assert_eq!(tx.signature.unwrap().0, 2);
		assert_eq!(
			tx.call,
			Call::Blueprints(pallet_asylum_blueprints::Call::<Test>::mint_item {
				owner: recipient_1,
				blueprint_id: 0,
				metadata: bounded("")
			})
		);
		let tx = pool_state.write().transactions.pop().unwrap();
		let tx = Extrinsic::decode(&mut &*tx).unwrap();
		assert_eq!(tx.signature.unwrap().0, 1);
		assert_eq!(
			tx.call,
			Call::Blueprints(pallet_asylum_blueprints::Call::<Test>::mint_item {
				owner: recipient_0,
				blueprint_id: 0,
				metadata: bounded("")
			})
		);
		assert_eq!(pool_state.read().transactions.len(), 0);
		assert!(MintRequests::<Test>::iter_values().collect::<Vec<_>>().is_empty());
	});
}
