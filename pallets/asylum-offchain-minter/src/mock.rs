use crate as asylum_offchain_minter;
use crate::*;
use frame_support::{
	parameter_types,
	traits::{AsEnsureOriginWithArg, ConstU32, ConstU64},
};

use frame_system as system;
use pallet_assets as assets;
use pallet_asylum_blueprints as blueprints;
use pallet_asylum_spaces as spaces;
use pallet_balances as balances;
use pallet_rmrk_core as rmrk;
use pallet_uniques as uniques;
use sp_core::{sr25519::Signature, H256};
use sp_runtime::{
	testing::{Header, TestXt},
	traits::{BlakeTwo256, Extrinsic as ExtrinsicT, IdentifyAccount, IdentityLookup, Verify},
};

pub type Extrinsic = TestXt<Call, ()>;
type UncheckedExtrinsic = frame_system::mocking::MockUncheckedExtrinsic<Test>;
type Block = frame_system::mocking::MockBlock<Test>;
type Balance = u64;
type AccountId = <<Signature as Verify>::Signer as IdentifyAccount>::AccountId;

// For testing the module, we construct a mock runtime.
frame_support::construct_runtime!(
	pub enum Test where
		Block = Block,
		NodeBlock = Block,
		UncheckedExtrinsic = UncheckedExtrinsic,
	{
		System: frame_system::{Pallet, Call, Config, Storage, Event<T>},
		Balances: balances::{Pallet, Call, Config<T>, Storage, Event<T>},
		Uniques: uniques::{Pallet, Call, Storage, Event<T>},
		RmrkCore: rmrk::{Pallet, Call, Storage, Event<T>},
		Blueprints: blueprints::{Pallet, Call, Storage, Event<T>},
		Assets: assets::{Pallet, Call, Storage, Event<T>},
		Spaces: spaces::{Pallet, Call, Storage, Event<T>},
		Minter: asylum_offchain_minter::{Pallet, Call, Storage, Event<T>},
	}
);

parameter_types! {
	pub const BlockHashCount: u64 = 250;
	pub const SS58Prefix: u8 = 42;
}

impl frame_system::Config for Test {
	type BaseCallFilter = frame_support::traits::Everything;
	type BlockWeights = ();
	type BlockLength = ();
	type DbWeight = ();
	type Origin = Origin;
	type Call = Call;
	type Index = u64;
	type BlockNumber = u64;
	type Hash = H256;
	type Hashing = BlakeTwo256;
	type AccountId = AccountId;
	type Lookup = IdentityLookup<Self::AccountId>;
	type Header = Header;
	type Event = Event;
	type BlockHashCount = BlockHashCount;
	type Version = ();
	type PalletInfo = PalletInfo;
	type AccountData = balances::AccountData<u64>;
	type OnNewAccount = ();
	type OnKilledAccount = ();
	type SystemWeightInfo = ();
	type SS58Prefix = SS58Prefix;
	type OnSetCode = ();
	type MaxConsumers = ConstU32<32>;
}

parameter_types! {
	pub const ExistentialDeposit: u64 = 1;
	pub const MaxLocks: u32 = 50;
}

impl pallet_balances::Config for Test {
	type MaxLocks = MaxLocks;
	type MaxReserves = ();
	type ReserveIdentifier = [u8; 8];
	type Balance = Balance;
	type Event = Event;
	type DustRemoval = ();
	type ExistentialDeposit = ExistentialDeposit;
	type AccountStore = System;
	type WeightInfo = ();
}

parameter_types! {
	pub const CollectionDeposit: Balance = 100;
	pub const ItemDeposit: Balance = 1;
	pub const KeyLimit: u32 = 32;
	pub const ValueLimit: u32 = 256;
	pub const UniquesMetadataDepositBase: Balance = 100;
	pub const AttributeDepositBase: Balance = 10;
	pub const DepositPerByte: Balance = 10;
	pub const UniquesStringLimit: u32 = 128;
}

impl pallet_uniques::Config for Test {
	type Event = Event;
	type CollectionId = u32;
	type ItemId = u32;
	type Currency = Balances;
	type CreateOrigin = AsEnsureOriginWithArg<frame_system::EnsureSigned<AccountId>>;
	type ForceOrigin = frame_system::EnsureRoot<AccountId>;
	type Locker = ();
	type CollectionDeposit = CollectionDeposit;
	type ItemDeposit = ItemDeposit;
	type MetadataDepositBase = UniquesMetadataDepositBase;
	type AttributeDepositBase = AttributeDepositBase;
	type DepositPerByte = DepositPerByte;
	type StringLimit = UniquesStringLimit;
	type KeyLimit = KeyLimit;
	type ValueLimit = ValueLimit;
	type WeightInfo = ();
}

parameter_types! {
	pub const MaxRecursions: u32 = 10;
	pub const ResourceSymbolLimit: u32 = 100;
	pub const CollectionSymbolLimit: u32 = 100;
	pub const TagLimit: u32 = 32;
	pub const MaxTags: u32 = 10;
	pub const MaxChanges: u32 = 10;
	pub const MaxAddChanges: u32 = 10;
	pub const MaxModifyChanges: u32 = 10;
	pub const MaxInterpretationsOnBlueprintCreate: u32 = 10;
	pub const PartsLimit: u32 = 10;
	pub const MaxPriorities: u32 = 10;
	pub const MaxResourcesOnMint: u32 = 10;
}

impl pallet_rmrk_core::Config for Test {
	type Event = Event;
	type ProtocolOrigin = frame_system::EnsureRoot<AccountId>;
	type MaxRecursions = MaxRecursions;
	type ResourceSymbolLimit = ResourceSymbolLimit;
	type CollectionSymbolLimit = CollectionSymbolLimit;
	type PartsLimit = PartsLimit;
	type MaxPriorities = MaxPriorities;
	type MaxResourcesOnMint = MaxResourcesOnMint;
}

impl pallet_asylum_blueprints::Config for Test {
	type Event = Event;
	type TagLimit = TagLimit;
	type MaxTags = MaxTags;
	type MaxChanges = MaxChanges;
	type MaxAddChanges = MaxAddChanges;
	type MaxModifyChanges = MaxModifyChanges;
	type MaxInterpretationsOnBlueprintCreate = MaxInterpretationsOnBlueprintCreate;
}

impl pallet_assets::Config for Test {
	type Event = Event;
	type Balance = u64;
	type AssetId = u32;
	type Currency = Balances;
	type ForceOrigin = frame_system::EnsureRoot<AccountId>;
	type AssetDeposit = ConstU64<1>;
	type AssetAccountDeposit = ConstU64<10>;
	type MetadataDepositBase = ConstU64<1>;
	type MetadataDepositPerByte = ConstU64<1>;
	type ApprovalDeposit = ConstU64<1>;
	type StringLimit = ConstU32<50>;
	type Freezer = ();
	type WeightInfo = ();
	type Extra = ();
}

impl pallet_asylum_spaces::Config for Test {
	type Event = Event;
	type Uniques = Uniques;
	type AssetId = u32;
	type Assets = Assets;
	type Currency = Balances;
	type DataLimit = ConstU32<50>;
	type StringLimit = ConstU32<50>;
	type KeyLimit = ConstU32<50>;
	type ValueLimit = ConstU32<50>;
	type MaxTeamMembers = ConstU32<3>;
	type MaxBlueprints = ConstU32<2>;
	type MaxAssets = ConstU32<2>;
}

impl frame_system::offchain::SigningTypes for Test {
	type Public = <Signature as Verify>::Signer;
	type Signature = Signature;
}

impl<LocalCall> frame_system::offchain::SendTransactionTypes<LocalCall> for Test
where
	Call: From<LocalCall>,
{
	type OverarchingCall = Call;
	type Extrinsic = Extrinsic;
}

impl<LocalCall> frame_system::offchain::CreateSignedTransaction<LocalCall> for Test
where
	Call: From<LocalCall>,
{
	fn create_transaction<C: frame_system::offchain::AppCrypto<Self::Public, Self::Signature>>(
		call: Call,
		_public: <Signature as Verify>::Signer,
		_account: AccountId,
		nonce: u64,
	) -> Option<(Call, <Extrinsic as ExtrinsicT>::SignaturePayload)> {
		Some((call, (nonce, ())))
	}
}

impl Config for Test {
	type Event = Event;
	type AuthorityId = sr25519::AuthorityId;
	type Call = Call;
	type MaxServers = ConstU32<2>;
}

pub struct ExtBuilder;
impl Default for ExtBuilder {
	fn default() -> Self {
		ExtBuilder
	}
}

impl ExtBuilder {
	pub fn build(self) -> sp_io::TestExternalities {
		let mut storage = system::GenesisConfig::default().build_storage::<Test>().unwrap();

		balances::GenesisConfig::<Test> {
			balances: vec![
				(sp_core::sr25519::Public::from_raw([1u8; 32]), 20_000_000),
				(sp_core::sr25519::Public::from_raw([2u8; 32]), 15_000),
			],
		}
		.assimilate_storage(&mut storage)
		.unwrap();

		let mut ext = sp_io::TestExternalities::new(storage);
		ext.execute_with(|| System::set_block_number(1));
		ext
	}
}
