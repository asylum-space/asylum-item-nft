#![cfg_attr(not(feature = "std"), no_std)]

use asylum_traits::primitives::{BlueprintId, SpaceId};
use codec::{Decode, Encode};
use frame_support::{
	traits::{tokens::nonfungibles::InspectEnumerable, Get},
	BoundedBTreeSet,
};
use frame_system::offchain::{AppCrypto, CreateSignedTransaction, SendSignedTransaction, Signer};
use scale_info::TypeInfo;
use sp_core::crypto::KeyTypeId;
use sp_runtime::{traits::Zero, RuntimeDebug};
use sp_std::{collections::btree_set::BTreeSet, vec, vec::Vec};

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

pub const ASYLUM: KeyTypeId = KeyTypeId(*b"_asy");

pub mod sr25519 {
	use sp_core::sr25519::Signature as Sr25519Signature;
	use sp_runtime::{traits::Verify, MultiSignature, MultiSigner};
	pub(crate) mod app_sr25519 {
		use crate::ASYLUM;
		use sp_runtime::app_crypto::{app_crypto, sr25519};
		app_crypto!(sr25519, ASYLUM);
	}

	pub struct AuthorityId;

	impl frame_system::offchain::AppCrypto<MultiSigner, MultiSignature> for AuthorityId {
		type RuntimeAppPublic = app_sr25519::Public;
		type GenericSignature = Sr25519Signature;
		type GenericPublic = sp_core::sr25519::Public;
	}

	// implemented for mock runtime in test
	impl frame_system::offchain::AppCrypto<<Sr25519Signature as Verify>::Signer, Sr25519Signature>
		for AuthorityId
	{
		type RuntimeAppPublic = app_sr25519::Public;
		type GenericSignature = Sr25519Signature;
		type GenericPublic = sp_core::sr25519::Public;
	}
}

pub use pallet::*;

#[derive(Encode, Decode, Eq, PartialEq, Clone, RuntimeDebug, TypeInfo)]
pub struct ItemMintRequest<AccountId, BoundedString> {
	pub item_recipient: AccountId,
	pub blueprint_id: BlueprintId,
	pub item_metadata: BoundedString,
}

#[frame_support::pallet]
pub mod pallet {
	use super::*;
	use frame_support::pallet_prelude::*;
	use frame_system::pallet_prelude::*;

	/// This pallet's configuration trait
	#[pallet::config]
	pub trait Config:
		CreateSignedTransaction<pallet_asylum_blueprints::Call<Self>>
		+ frame_system::Config
		+ pallet_asylum_blueprints::Config
		+ pallet_asylum_spaces::Config
		+ pallet_uniques::Config<CollectionId = u32, ItemId = u32>
	{
		type AuthorityId: AppCrypto<Self::Public, Self::Signature>;
		type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
		type Call: From<Call<Self>>;

		#[pallet::constant]
		type MaxServers: Get<u32>;
	}

	pub type ItemMintRequestOf<T> = ItemMintRequest<
		<T as frame_system::Config>::AccountId,
		pallet_asylum_blueprints::StringLimitOf<T>,
	>;

	#[pallet::pallet]
	#[pallet::without_storage_info]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	#[pallet::storage]
	#[pallet::getter(fn mint_requests)]
	#[pallet::unbounded]
	pub(super) type MintRequests<T: Config> =
		StorageMap<_, Blake2_128Concat, T::AccountId, Vec<ItemMintRequestOf<T>>, OptionQuery>;

	#[pallet::storage]
	#[pallet::getter(fn space_server_accounts)]
	pub(super) type SpaceServerAccounts<T: Config> = StorageMap<
		_,
		Blake2_128Concat,
		SpaceId,
		BoundedBTreeSet<T::AccountId, T::MaxServers>,
		ValueQuery,
	>;

	#[pallet::hooks]
	impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {
		fn offchain_worker(_block_number: T::BlockNumber) {
			let res = Self::process_requests();
			if let Err(e) = res {
				log::error!("Error: {}", e);
			}
		}
	}

	/// A public part of the pallet.
	#[pallet::call]
	impl<T: Config> Pallet<T> {
		/// Space's server account request the item mint
		///
		/// # Arguments
		///
		/// * `item_recipient` - item's recipient
		/// * `space_id` - space associated with this mint request
		/// * `blueprint_id` - mint item from this blueprint
		/// * `item_metadata` - item's metadata
		#[pallet::weight(0)]
		pub fn request_item_mint(
			origin: OriginFor<T>,
			item_recipient: T::AccountId,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] blueprint_id: BlueprintId,
			item_metadata: pallet_asylum_blueprints::StringLimitOf<T>,
		) -> DispatchResultWithPostInfo {
			let server = ensure_signed(origin)?;
			ensure!(
				Self::space_server_accounts(space_id).contains(&server),
				Error::<T>::NoAccessToMinting
			);
			ensure!(
				!pallet_asylum_spaces::Pallet::<T>::owned_in_collection(&space_id, &item_recipient)
					.count()
					.is_zero(),
				Error::<T>::NoAccessToSpace
			);
			// Space existence is indirectly checked by space_server_accounts()
			ensure!(
				pallet_asylum_spaces::Pallet::<T>::spaces(space_id)
					.unwrap()
					.blueprints
					.ok_or(Error::<T>::SpaceDoesntSupportBlueprint)?
					.contains(&blueprint_id),
				Error::<T>::SpaceDoesntSupportBlueprint
			);
			let mint_request = ItemMintRequest { item_recipient, blueprint_id, item_metadata };
			MintRequests::<T>::try_mutate(server, |maybe_requests| -> DispatchResult {
				if let Some(requests) = maybe_requests {
					requests.push(mint_request);
				} else {
					*maybe_requests = Some(vec![mint_request]);
				}
				Ok(())
			})?;
			Ok(().into())
		}

		/// Associate set of account with space. These servers can request minting
		///
		/// # Arguments
		///
		/// * `space_id` - space associated with this mint request
		/// * `servers` - server's set
		#[pallet::weight(0)]
		pub fn set_space_servers(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			servers: BTreeSet<T::AccountId>,
		) -> DispatchResultWithPostInfo {
			ensure_root(origin)?;
			ensure!(
				pallet_asylum_spaces::Pallet::<T>::spaces(space_id).is_some(),
				Error::<T>::SpaceDoesntExist
			);
			let servers: BoundedBTreeSet<_, T::MaxServers> =
				servers.try_into().map_err(|_| Error::<T>::ServerLimitExceeded)?;
			SpaceServerAccounts::<T>::insert(space_id, servers);
			Ok(().into())
		}
	}

	/// Events for the pallet.
	#[pallet::event]
	pub enum Event<T: Config> {}

	#[pallet::error]
	pub enum Error<T> {
		NoAccessToSpace,
		NoAccessToMinting,
		SpaceDoesntSupportBlueprint,
		SpaceDoesntExist,
		ServerLimitExceeded,
	}
}

impl<T: Config> Pallet<T> {
	fn process_requests() -> Result<(), &'static str> {
		MintRequests::<T>::drain().try_for_each(|(_, requests)| -> Result<(), &'static str> {
			requests.into_iter().try_for_each(move |req| -> Result<(), &'static str> {
				Self::mint_item(req)?;
				Ok(())
			})
		})
	}
	/// A helper function to fetch the price and send signed transaction.
	fn mint_item(mint_request: ItemMintRequestOf<T>) -> Result<(), &'static str> {
		let signer = Signer::<T, T::AuthorityId>::all_accounts();
		if !signer.can_sign() {
			return Err(
				"No local accounts available. Consider adding one via `author_insertKey` RPC.",
			)
		}
		let results = signer.send_signed_transaction(|_account| pallet_asylum_blueprints::Call::<
			T,
		>::mint_item {
			owner: mint_request.item_recipient.clone(),
			blueprint_id: mint_request.blueprint_id,
			metadata: mint_request.item_metadata.clone(),
		});

		for (acc, res) in &results {
			match res {
				Ok(()) => log::info!("Mint item to [{:?}]", mint_request.item_recipient),
				Err(_e) => log::error!("[{:?}] Failed to submit transaction", acc.id),
			}
		}

		Ok(())
	}
}
