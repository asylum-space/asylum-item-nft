# Asylum Spaces

A simple, secure module for dealing with Asylum spaces and space pass'

## Overview

The Asylum GDS module is based on `pallet_uniques`. This module provides functionality for spaces and space pass' management, including:

* Space creation
* Space destroying
* SpacePass issuance
* SpacePass transfer
* SpacePass burning

### Flow diagram

![](/docs/img/asylum-flow-diagram.png)

### Terminology

* **Space:** The `Space` consists of: 
  - description metadata,
  - runnable `Space Client`,
  - set of admins (or DAO) and owner who can modify the space,
  - _[planed]_ on-chain state and space back-end (probably TEE), which modifies the state.
* **SpacePass:** The NFT, which is used as a pass to the `Game`.
* **Game Client:** The binary (e. g. WASM), which the Player uses to run and play the `Game`. Right now, it will be a link to the server where the space is spun up.


## Interface

### Game dispatchables
* `create_space`: Create a new space.
* `destroy_space`: Destroy a space.
* `freeze_space`: Prevent all space_passs within a space from being transferred.
* `thaw_space`: Revert the effects of a previous `freeze_space`.
* `transfer_space_ownership`: Alter the owner of a space.
* `set_team`: Alter the permissioned accounts of a space.

### SpacePass dispatchables
* `mint_space_pass`: Mint a new space_pass within an asset class.
* `transfer_space_pass`: Transfer a space_pass to a new owner.
* `burn_space_pass`: Burn a space_pass within a space.
* `freeze_space_pass`: Prevent a space_pass from being transferred.
* `thaw_space_pass`: Revert the effects of a previous `freeze_space_pass`.
* `approve_transfer`: Assign a delegator who can authorize a transfer.
* `cancel_approval`: Revert the effects of a previous `approve_transfer`.

### Metadata (permissioned) dispatchables
* `set_attribute`: Set a metadata attribute of a space_pass or space.
* `clear_attribute`: Remove a metadata attribute of a space_pass or space.
* `set_space_pass_metadata`: Set general metadata of a tieckt.
* `clear_space_pass_metadata`: Remove general metadata of a space_pass.
* `set_space_metadata`: Set general metadata of a space.
* `clear_space_metadata`: Remove general metadata of aspace.

## Related Modules

* [`System`](https://docs.rs/frame-system/latest/frame_system/)
* [`Support`](https://docs.rs/frame-support/latest/frame_support/)
* [`Uniques`](https://paritytech.github.io/substrate/master/pallet_uniques/index.html)

License: MIT
