use super::*;
use crate::mock::*;
use frame_support::{
	assert_noop, assert_ok,
	traits::{Currency, Get},
	BoundedVec,
};
use sp_std::prelude::*;

fn space_passes() -> Vec<(u64, u32, u32)> {
	let mut r: Vec<_> = Account::<Test>::iter().map(|x| x.0).collect();
	r.sort_unstable();
	let mut s: Vec<_> = SpacePass::<Test>::iter().map(|x| (x.2.owner, x.0, x.1)).collect();
	s.sort_unstable();
	assert_eq!(r, s);
	for class in SpacePass::<Test>::iter()
		.map(|x| x.0)
		.scan(None, |s, item| {
			if s.map_or(false, |last| last == item) {
				*s = Some(item);
				Some(None)
			} else {
				Some(Some(item))
			}
		})
		.flatten()
	{
		let details = Space::<Test>::get(class).unwrap();
		let instances = SpacePass::<Test>::iter_prefix(class).count() as u32;
		assert_eq!(details.instances, instances);
	}
	r
}

fn spaces() -> Vec<(u64, u32)> {
	let mut r: Vec<_> = SpaceAccount::<Test>::iter().map(|x| (x.0, x.1)).collect();
	r.sort_unstable();
	let mut s: Vec<_> = Space::<Test>::iter().map(|x| (x.1.owner, x.0)).collect();
	s.sort_unstable();
	assert_eq!(r, s);
	r
}

macro_rules! bvec {
	($( $x:tt )*) => {
		vec![$( $x )*].try_into().unwrap()
	}
}

fn attributes(class: u32) -> Vec<(Option<u32>, Vec<u8>, Vec<u8>)> {
	let mut s: Vec<_> = Attribute::<Test>::iter_prefix((class,))
		.map(|(k, v)| (k.0, k.1.into(), v.into()))
		.collect();
	s.sort();
	s
}

fn bounded<T>(string: &str) -> BoundedVec<u8, T>
where
	T: Get<u32>,
{
	TryInto::<BoundedVec<u8, T>>::try_into(string.as_bytes().to_vec()).unwrap()
}

#[test]
fn basic_setup_works() {
	new_test_ext().execute_with(|| {
		assert_eq!(space_passes(), vec![]);
	});
}

#[test]
fn basic_minting_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_eq!(spaces(), vec![(1, 0)]);
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(1), 0, true));
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(2), 0, 0, 1),
			pallet_balances::Error::<Test>::InsufficientBalance
		);
		Balances::make_free_balance_be(&2, 1001);
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 0, 1));
		assert_noop!(Spaces::mint_space_pass(Origin::signed(2), 42, 0, 1), Error::<Test>::Unknown);
		assert_eq!(space_passes(), vec![(1, 0, 0)]);
		assert_eq!(Balances::free_balance(&1), 1000);
		assert_eq!(Balances::free_balance(&2), 1);

		assert_ok!(Spaces::create_space(Origin::signed(2), 1, vec![2], Some(1000)));
		assert_eq!(spaces(), vec![(1, 0), (2, 1)]);
		// free mint because space owner and minter are the same account
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 1, 0, 1));
		assert_eq!(space_passes(), vec![(1, 0, 0), (1, 1, 0)]);
	});
}

#[test]
fn lifecycle_should_work() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&1, 100);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_eq!(spaces(), vec![(1, 0)]);
		assert_ok!(Spaces::set_space_metadata(
			Origin::signed(1),
			0,
			bounded("ipfs://"),
			bounded("my space"),
			bounded("battle royal")
		));
		assert!(SpaceMetadataOf::<Test>::contains_key(0));

		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 10));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 1, 20));
		assert_eq!(space_passes(), vec![(10, 0, 0), (20, 0, 1)]);
		assert_eq!(Space::<Test>::get(0).unwrap().instances, 2);
		assert_eq!(Space::<Test>::get(0).unwrap().instance_metadatas, 0);

		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 0, bvec![42, 42],));
		assert!(SpacePassMetadataOf::<Test>::contains_key(0, 0));
		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 1, bvec![69, 69],));
		assert!(SpacePassMetadataOf::<Test>::contains_key(0, 1));

		let w = Space::<Test>::get(0).unwrap().destroy_witness();
		assert_eq!(w.instances, 2);
		assert_eq!(w.instance_metadatas, 2);
		assert_ok!(Spaces::destroy_space(Origin::signed(1), 0, w));

		assert!(!Space::<Test>::contains_key(0));
		assert!(!SpacePass::<Test>::contains_key(0, 0));
		assert!(!SpacePass::<Test>::contains_key(0, 1));
		assert!(!SpaceMetadataOf::<Test>::contains_key(0));
		assert!(!SpacePassMetadataOf::<Test>::contains_key(0, 0));
		assert!(!SpacePassMetadataOf::<Test>::contains_key(0, 1));
		assert_eq!(spaces(), vec![]);
		assert_eq!(space_passes(), vec![]);
	});
}

#[test]
fn destroy_with_bad_witness_should_not_work() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&1, 100);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));

		let w = Space::<Test>::get(0).unwrap().destroy_witness();
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));
		assert_noop!(Spaces::destroy_space(Origin::signed(1), 0, w), Error::<Test>::BadWitness);
	});
}

#[test]
fn mint_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));

		Balances::make_free_balance_be(&2, 1001);
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(2), 0, 1, 2),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::set_allow_unpriviledged_mint(Origin::signed(2), 0, true),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(1), 0, true));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 1, 2));
		assert_eq!(Spaces::owner(0, 0).unwrap(), 1);
		assert_eq!(spaces(), vec![(1, 0)]);
		assert_eq!(space_passes(), vec![(1, 0, 0), (2, 0, 1)]);
	});
}

#[test]
fn transfer_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 2));

		assert_ok!(Spaces::transfer(Origin::signed(2), 0, 0, 3));
		assert_eq!(space_passes(), vec![(3, 0, 0)]);
		assert_noop!(Spaces::transfer(Origin::signed(2), 0, 0, 4), Error::<Test>::NoPermission);

		assert_ok!(Spaces::approve_transfer(Origin::signed(3), 0, 0, 2));
		assert_ok!(Spaces::transfer(Origin::signed(2), 0, 0, 4));
	});
}

#[test]
fn freezing_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));
		assert_ok!(Spaces::freeze_space_pass(Origin::signed(1), 0, 0));
		assert_noop!(Spaces::freeze_space_pass(Origin::signed(1), 0, 42), Error::<Test>::Unknown);
		assert_noop!(Spaces::transfer(Origin::signed(1), 0, 0, 2), Error::<Test>::Frozen);

		assert_ok!(Spaces::thaw_space_pass(Origin::signed(1), 0, 0));
		assert_noop!(Spaces::thaw_space_pass(Origin::signed(1), 0, 42), Error::<Test>::Unknown);

		assert_ok!(Spaces::freeze_space(Origin::signed(1), 0));
		assert_noop!(Spaces::freeze_space(Origin::signed(2), 0), Error::<Test>::NoPermission);
		assert_noop!(Spaces::freeze_space(Origin::signed(1), 42), Error::<Test>::Unknown);
		assert_noop!(Spaces::transfer(Origin::signed(1), 0, 0, 2), Error::<Test>::Frozen);

		assert_ok!(Spaces::thaw_space(Origin::signed(1), 0));
		assert_noop!(Spaces::thaw_space(Origin::signed(2), 0), Error::<Test>::NoPermission);
		assert_noop!(Spaces::thaw_space(Origin::signed(1), 42), Error::<Test>::Unknown);
		assert_noop!(Spaces::thaw_space(Origin::signed(1), 42), Error::<Test>::Unknown);
		assert_ok!(Spaces::transfer(Origin::signed(1), 0, 0, 2));
	});
}

#[test]
fn origin_guards_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(1), 0, true));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));
		assert_noop!(
			Spaces::transfer_space_ownership(Origin::signed(2), 0, 2),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::set_space_team(
				Origin::signed(2),
				0,
				vec![2].try_into().unwrap(),
				vec![2].try_into().unwrap(),
				vec![2].try_into().unwrap()
			),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::freeze_space_pass(Origin::signed(2), 0, 0),
			Error::<Test>::NoPermission
		);
		assert_noop!(Spaces::thaw_space_pass(Origin::signed(2), 0, 0), Error::<Test>::NoPermission);
		Balances::make_free_balance_be(&2, 1001);
		// everybody can mint space_passes
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 1, 2));
		assert_noop!(
			Spaces::burn_space_pass(Origin::signed(2), 0, 0, None),
			Error::<Test>::NoPermission
		);
		let w = Space::<Test>::get(0).unwrap().destroy_witness();
		assert_noop!(Spaces::destroy_space(Origin::signed(2), 0, w), Error::<Test>::NoPermission);
	});
}

#[test]
fn transfer_owner_should_work() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&1, 100);
		Balances::make_free_balance_be(&2, 100);
		Balances::make_free_balance_be(&3, 100);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(99)));
		assert_eq!(spaces(), vec![(1, 0)]);
		assert_ok!(Spaces::transfer_space_ownership(Origin::signed(1), 0, 2));
		// Self transfer is Ok
		assert_ok!(Spaces::transfer_space_ownership(Origin::signed(2), 0, 2));
		assert_eq!(spaces(), vec![(2, 0)]);

		assert_noop!(
			Spaces::transfer_space_ownership(Origin::signed(1), 0, 1),
			Error::<Test>::NoPermission
		);

		// Mint and set metadata now and make sure that deposit gets transferred back.
		assert_ok!(Spaces::set_space_metadata(
			Origin::signed(2),
			0,
			bounded("ipfs://"),
			bounded("my space"),
			bounded("battle royal")
		));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));
		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(2), 0, 0, bounded("ipfs://")));
		assert_ok!(Spaces::transfer_space_ownership(Origin::signed(2), 0, 3));
		assert_eq!(spaces(), vec![(3, 0)]);
	});
}

#[test]
fn set_team_should_work() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&2, 1001);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::set_space_team(
			Origin::signed(1),
			0,
			vec![2].try_into().unwrap(),
			vec![3].try_into().unwrap(),
			vec![4].try_into().unwrap()
		));

		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 0, 2));
		assert_ok!(Spaces::freeze_space_pass(Origin::signed(4), 0, 0));
		assert_ok!(Spaces::thaw_space_pass(Origin::signed(3), 0, 0));
		assert_ok!(Spaces::transfer(Origin::signed(3), 0, 0, 3));
		assert_ok!(Spaces::burn_space_pass(Origin::signed(3), 0, 0, None));
	});
}

#[test]
fn set_space_metadata_should_work() {
	new_test_ext().execute_with(|| {
		// Cannot add metadata to unknown asset
		assert_noop!(
			Spaces::set_space_metadata(
				Origin::signed(1),
				0,
				bounded("ipfs://"),
				bounded("my space"),
				bounded("battle royal")
			),
			Error::<Test>::Unknown,
		);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		// Cannot add metadata to unowned asset
		assert_noop!(
			Spaces::set_space_metadata(
				Origin::signed(2),
				0,
				bounded("ipfs://"),
				bounded("my space"),
				bounded("battle royal")
			),
			Error::<Test>::NoPermission,
		);

		// Successfully add metadata
		assert_ok!(Spaces::set_space_metadata(
			Origin::signed(1),
			0,
			bounded("ipfs://new"),
			bounded("my space"),
			bounded("battle royal")
		));
		assert!(SpaceMetadataOf::<Test>::contains_key(0));

		assert_ok!(Spaces::set_space_metadata(
			Origin::signed(1),
			0,
			bounded("ipfs://"),
			bounded("my space"),
			bounded("battle royal")
		));

		assert_noop!(
			Spaces::clear_space_metadata(Origin::signed(2), 0),
			Error::<Test>::NoPermission
		);
		assert_noop!(Spaces::clear_space_metadata(Origin::signed(1), 1), Error::<Test>::Unknown);
		assert_ok!(Spaces::clear_space_metadata(Origin::signed(1), 0));
		assert!(!SpaceMetadataOf::<Test>::contains_key(0));
	});
}

#[test]
fn set_space_pass_metadata_should_work() {
	new_test_ext().execute_with(|| {
		// Cannot add metadata to unknown asset
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 1));
		// Cannot add metadata to unowned asset
		assert_noop!(
			Spaces::set_space_pass_metadata(Origin::signed(2), 0, 0, bvec![0u8; 20]),
			Error::<Test>::NoPermission,
		);
		assert_noop!(
			Spaces::set_space_pass_metadata(Origin::signed(2), 42, 0, bvec![0u8; 20]),
			Error::<Test>::Unknown,
		);
		// Successfully add metadata and take deposit
		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 0, bvec![0u8; 20],));
		assert!(SpacePassMetadataOf::<Test>::contains_key(0, 0));

		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 0, bvec![0u8; 15],));
		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 0, bvec![0u8; 25],));

		// Clear Metadata
		assert_ok!(Spaces::set_space_pass_metadata(Origin::signed(1), 0, 0, bvec![0u8; 15],));
		assert_noop!(
			Spaces::clear_space_pass_metadata(Origin::signed(2), 0, 0),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::clear_space_pass_metadata(Origin::signed(1), 1, 0),
			Error::<Test>::Unknown
		);
		assert_ok!(Spaces::clear_space_pass_metadata(Origin::signed(1), 0, 0));
		assert!(!SpacePassMetadataOf::<Test>::contains_key(0, 0));
	});
}

#[test]
fn set_attribute_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));

		assert_ok!(Spaces::set_attribute(Origin::signed(1), 0, None, bvec![0], bvec![0]));
		assert_noop!(
			Spaces::set_attribute(Origin::signed(0), 42, None, bvec![0], bvec![0]),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::set_attribute(Origin::signed(42), 0, None, bvec![0], bvec![0]),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::set_attribute(Origin::signed(1), 0, Some(0), bvec![0], bvec![0]));
		assert_ok!(Spaces::set_attribute(Origin::signed(1), 0, Some(0), bvec![1], bvec![0]));
		assert_eq!(
			attributes(0),
			vec![
				(None, bvec![0], bvec![0]),
				(Some(0), bvec![0], bvec![0]),
				(Some(0), bvec![1], bvec![0]),
			]
		);

		assert_ok!(Spaces::set_attribute(Origin::signed(1), 0, None, bvec![0], bvec![0; 10]));
		assert_eq!(
			attributes(0),
			vec![
				(None, bvec![0], bvec![0; 10]),
				(Some(0), bvec![0], bvec![0]),
				(Some(0), bvec![1], bvec![0]),
			]
		);

		assert_ok!(Spaces::clear_attribute(Origin::signed(1), 0, Some(0), bvec![1]));
		assert_noop!(
			Spaces::clear_attribute(Origin::signed(0), 42, None, bvec![0]),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::clear_attribute(Origin::signed(42), 0, None, bvec![0]),
			Error::<Test>::NoPermission
		);
		assert_eq!(
			attributes(0),
			vec![(None, bvec![0], bvec![0; 10]), (Some(0), bvec![0], bvec![0]),]
		);

		let w = Space::<Test>::get(0).unwrap().destroy_witness();
		assert_ok!(Spaces::destroy_space(Origin::signed(1), 0, w));
		assert_eq!(attributes(0), vec![]);
	});
}

#[test]
fn burn_works() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&1, 100);
		Balances::make_free_balance_be(&2, 201);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(100)));
		assert_ok!(Spaces::set_space_team(
			Origin::signed(1),
			0,
			vec![2].try_into().unwrap(),
			vec![3].try_into().unwrap(),
			vec![4].try_into().unwrap()
		));

		assert_noop!(
			Spaces::burn_space_pass(Origin::signed(5), 0, 42, Some(5)),
			Error::<Test>::Unknown
		);
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(1), 0, true));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 0, 5));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 1, 5));

		assert_noop!(
			Spaces::burn_space_pass(Origin::signed(0), 0, 0, None),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::burn_space_pass(Origin::signed(5), 0, 0, Some(6)),
			Error::<Test>::WrongOwner
		);

		assert_ok!(Spaces::burn_space_pass(Origin::signed(5), 0, 0, Some(5)));
		assert_ok!(Spaces::burn_space_pass(Origin::signed(3), 0, 1, Some(5)));
	});
}

#[test]
fn approval_lifecycle_works() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 2));
		assert_ok!(Spaces::approve_transfer(Origin::signed(2), 0, 0, 3));
		assert_noop!(Spaces::approve_transfer(Origin::signed(2), 42, 0, 3), Error::<Test>::Unknown);
		assert_noop!(Spaces::approve_transfer(Origin::signed(2), 0, 42, 3), Error::<Test>::Unknown);
		assert_noop!(
			Spaces::approve_transfer(Origin::signed(42), 0, 0, 3),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::transfer(Origin::signed(3), 0, 0, 4));
		assert_noop!(Spaces::transfer(Origin::signed(3), 0, 0, 3), Error::<Test>::NoPermission);
		assert!(SpacePass::<Test>::get(0, 0).unwrap().approved.is_none());

		assert_ok!(Spaces::approve_transfer(Origin::signed(4), 0, 0, 2));
		assert_ok!(Spaces::transfer(Origin::signed(2), 0, 0, 2));
	});
}

#[test]
fn cancel_approval_works() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 2));

		assert_ok!(Spaces::approve_transfer(Origin::signed(2), 0, 0, 3));
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(2), 1, 0, None),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(2), 0, 1, None),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(3), 0, 0, None),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(2), 0, 0, Some(4)),
			Error::<Test>::WrongDelegate
		);

		assert_ok!(Spaces::cancel_approval(Origin::signed(2), 0, 0, Some(3)));
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(2), 0, 0, None),
			Error::<Test>::NoDelegate
		);
	});
}

#[test]
fn cancel_approval_works_with_admin() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 0, 2));

		assert_ok!(Spaces::approve_transfer(Origin::signed(2), 0, 0, 3));
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(1), 1, 0, None),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(1), 0, 1, None),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(1), 0, 0, Some(4)),
			Error::<Test>::WrongDelegate
		);

		assert_ok!(Spaces::cancel_approval(Origin::signed(1), 0, 0, Some(3)));
		assert_noop!(
			Spaces::cancel_approval(Origin::signed(1), 0, 0, None),
			Error::<Test>::NoDelegate
		);
	});
}

#[test]
fn blueprints_support() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&2, 1000);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Uniques::create(Origin::signed(2), 101, 2));
		assert_ok!(Uniques::create(Origin::signed(2), 102, 2));
		assert_ok!(Uniques::create(Origin::signed(2), 103, 2));
		assert_noop!(
			Spaces::add_blueprint_support(Origin::signed(1), 0, 100),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::add_blueprint_support(Origin::signed(42), 0, 101),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::add_blueprint_support(Origin::signed(1), 0, 101));
		assert_ok!(Spaces::add_blueprint_support(Origin::signed(1), 0, 102));
		assert_noop!(
			Spaces::add_blueprint_support(Origin::signed(1), 0, 103),
			Error::<Test>::Error
		);
		assert_eq!(
			Space::<Test>::get(0).unwrap().blueprints,
			Some(BTreeSet::from([101, 102]).try_into().unwrap())
		);
		assert_noop!(
			Spaces::remove_blueprint_support(Origin::signed(42), 0, 101),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::remove_blueprint_support(Origin::signed(1), 0, 101));
		assert_ok!(Spaces::remove_blueprint_support(Origin::signed(1), 0, 102));
		assert_eq!(
			Space::<Test>::get(0).unwrap().blueprints,
			Some(BTreeSet::default().try_into().unwrap())
		);
	});
}

#[test]
fn assets_support() {
	new_test_ext().execute_with(|| {
		Balances::make_free_balance_be(&1, 1000);
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], Some(1000)));
		assert_ok!(Assets::create(Origin::signed(1), 101, 1, 1));
		assert_ok!(Assets::create(Origin::signed(1), 102, 1, 1));
		assert_ok!(Assets::create(Origin::signed(1), 103, 1, 1));
		assert_ok!(Assets::mint(Origin::signed(1), 101, 1, 1000));
		assert_ok!(Assets::mint(Origin::signed(1), 102, 1, 1000));
		assert_ok!(Assets::mint(Origin::signed(1), 103, 1, 1000));
		assert_noop!(
			Spaces::add_blueprint_support(Origin::signed(1), 0, 100),
			Error::<Test>::Unknown
		);
		assert_noop!(
			Spaces::add_asset_support(Origin::signed(42), 0, 101),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::add_asset_support(Origin::signed(1), 0, 101));
		assert_ok!(Spaces::add_asset_support(Origin::signed(1), 0, 102));
		assert_noop!(Spaces::add_asset_support(Origin::signed(1), 0, 103), Error::<Test>::Error);
		assert_noop!(Spaces::add_asset_support(Origin::signed(1), 0, 104), Error::<Test>::Unknown);
		assert_eq!(
			Space::<Test>::get(0).unwrap().assets,
			Some(BTreeSet::from([101, 102]).try_into().unwrap())
		);
		assert_noop!(
			Spaces::remove_asset_support(Origin::signed(42), 0, 101),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::remove_asset_support(Origin::signed(1), 0, 101));
		assert_ok!(Spaces::remove_asset_support(Origin::signed(1), 0, 102));
		assert_eq!(
			Space::<Test>::get(0).unwrap().assets,
			Some(BTreeSet::default().try_into().unwrap())
		);
	});
}

#[test]
fn set_price() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![1], None));
		assert_ok!(Spaces::set_allow_unpriviledged_mint(Origin::signed(1), 0, true));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 0, 2));
		assert_ok!(Spaces::set_price(Origin::signed(1), 0, 100));
		assert_noop!(Spaces::set_price(Origin::signed(42), 0, 100), Error::<Test>::NoPermission);
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(2), 0, 1, 2),
			pallet_balances::Error::<Test>::InsufficientBalance
		);
		Balances::make_free_balance_be(&2, 101);
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 1, 2));
	});
}

#[test]
fn several_admins_test() {
	new_test_ext().execute_with(|| {
		assert_ok!(Spaces::create_space(Origin::signed(1), 0, vec![2, 3], None));
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(4), 0, 0, 2),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::mint_space_pass(Origin::signed(2), 0, 0, 2));
		assert_ok!(Spaces::mint_space_pass(Origin::signed(3), 0, 1, 3));
		assert_ok!(Spaces::set_space_team(
			Origin::signed(1),
			0,
			vec![1].try_into().unwrap(),
			vec![2].try_into().unwrap(),
			vec![3].try_into().unwrap()
		));
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(2), 0, 2, 2),
			Error::<Test>::NoPermission
		);
		assert_noop!(
			Spaces::mint_space_pass(Origin::signed(3), 0, 2, 3),
			Error::<Test>::NoPermission
		);
		assert_ok!(Spaces::mint_space_pass(Origin::signed(1), 0, 2, 1));
	});
}
