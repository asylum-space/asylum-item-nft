use super::*;
use frame_support::{ensure, BoundedBTreeSet};
use sp_runtime::{DispatchError, DispatchResult};
use sp_std::collections::btree_set::BTreeSet;

impl<T: Config> Pallet<T> {
	pub fn do_transfer(
		space_id: SpaceId,
		space_pass_id: SpacePassId,
		dest: T::AccountId,
		with_details: impl FnOnce(&SpaceDetailsFor<T>, &mut SpacePassDetailsFor<T>) -> DispatchResult,
	) -> DispatchResult {
		let space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
		ensure!(!space_details.is_frozen, Error::<T>::Frozen);

		let mut details =
			SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;
		ensure!(!details.is_frozen, Error::<T>::Frozen);
		with_details(&space_details, &mut details)?;

		Account::<T>::remove((&details.owner, &space_id, &space_pass_id));
		Account::<T>::insert((&dest, &space_id, &space_pass_id), ());
		let origin = details.owner;
		details.owner = dest;
		SpacePass::<T>::insert(&space_id, &space_pass_id, &details);

		Self::deposit_event(Event::SpacePassTransferred {
			space_id,
			space_pass_id,
			from: origin,
			to: details.owner,
		});
		Ok(())
	}

	pub fn do_create_space(
		space_id: SpaceId,
		owner: T::AccountId,
		admins: BTreeSet<T::AccountId>,
		price: Option<BalanceOf<T>>,
	) -> DispatchResult {
		let event = Event::SpaceCreated { space_id, owner: owner.clone(), admins: admins.clone() };
		let admins: BoundedBTreeSet<_, _> = admins.try_into().map_err(|_| Error::<T>::Error)?;
		Space::<T>::insert(
			space_id,
			SpaceDetails {
				owner: owner.clone(),
				issuers: admins.clone(),
				admins: admins.clone(),
				freezers: admins,
				price,
				instances: 0,
				instance_metadatas: 0,
				attributes: 0,
				is_frozen: false,
				blueprints: None,
				assets: None,
				allow_unprivileged_mint: false,
			},
		);

		SpaceAccount::<T>::insert(&owner, &space_id, ());
		Self::deposit_event(event);
		Ok(())
	}

	pub fn do_destroy_space(
		space_id: SpaceId,
		witness: DestroyWitness,
		maybe_check_owner: Option<T::AccountId>,
	) -> Result<DestroyWitness, DispatchError> {
		Space::<T>::try_mutate_exists(space_id, |maybe_details| {
			let space_details = maybe_details.take().ok_or(Error::<T>::Unknown)?;
			if let Some(check_owner) = maybe_check_owner {
				ensure!(space_details.owner == check_owner, Error::<T>::NoPermission);
			}
			ensure!(space_details.instances == witness.instances, Error::<T>::BadWitness);
			ensure!(
				space_details.instance_metadatas == witness.instance_metadatas,
				Error::<T>::BadWitness
			);
			ensure!(space_details.attributes == witness.attributes, Error::<T>::BadWitness);

			for (instance, details) in SpacePass::<T>::drain_prefix(&space_id) {
				Account::<T>::remove((&details.owner, &space_id, &instance));
			}
			SpacePassMetadataOf::<T>::remove_prefix(&space_id, None);
			SpaceMetadataOf::<T>::remove(&space_id);
			Attribute::<T>::remove_prefix((&space_id,), None);
			SpaceAccount::<T>::remove(&space_details.owner, &space_id);

			Self::deposit_event(Event::SpaceDestroyed { space_id });

			Ok(DestroyWitness {
				instances: space_details.instances,
				instance_metadatas: space_details.instance_metadatas,
				attributes: space_details.attributes,
			})
		})
	}

	pub fn do_mint_space_pass(
		space_id: SpaceId,
		space_pass_id: SpacePassId,
		owner: T::AccountId,
		with_details: impl FnOnce(&SpaceDetailsFor<T>) -> DispatchResult,
	) -> DispatchResult {
		Space::<T>::try_mutate(&space_id, |maybe_space_details| -> DispatchResult {
			let space_details = maybe_space_details.as_mut().ok_or(Error::<T>::Unknown)?;

			with_details(space_details)?;

			let instances =
				space_details.instances.checked_add(1).ok_or(ArithmeticError::Overflow)?;
			space_details.instances = instances;

			let owner = owner.clone();
			Account::<T>::insert((&owner, &space_id, &space_pass_id), ());
			let details = SpacePassDetails { owner, approved: None, is_frozen: false };
			SpacePass::<T>::insert(&space_id, &space_pass_id, details);
			Ok(())
		})?;

		Self::deposit_event(Event::SpacePassIssued { space_id, space_pass_id, owner });
		Ok(())
	}

	pub fn do_burn_space_pass(
		space_id: SpaceId,
		space_pass_id: SpacePassId,
		with_details: impl FnOnce(&SpaceDetailsFor<T>, &SpacePassDetailsFor<T>) -> DispatchResult,
	) -> DispatchResult {
		let owner = Space::<T>::try_mutate(
			&space_id,
			|maybe_class_details| -> Result<T::AccountId, DispatchError> {
				let space_details = maybe_class_details.as_mut().ok_or(Error::<T>::Unknown)?;
				let details =
					SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;
				with_details(space_details, &details)?;

				space_details.instances.saturating_dec();
				Ok(details.owner)
			},
		)?;

		SpacePass::<T>::remove(&space_id, &space_pass_id);
		Account::<T>::remove((&owner, &space_id, &space_pass_id));

		Self::deposit_event(Event::SpacePassBurned { space_id, space_pass_id, owner });
		Ok(())
	}
}
