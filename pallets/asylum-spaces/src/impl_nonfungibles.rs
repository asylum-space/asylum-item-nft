use super::*;
use frame_support::{traits::tokens::nonfungibles::*, BoundedSlice};
use sp_runtime::{DispatchError, DispatchResult};
use sp_std::prelude::*;

impl<T: Config> Inspect<<T as SystemConfig>::AccountId> for Pallet<T> {
	type ItemId = SpacePassId;
	type CollectionId = SpaceId;

	fn owner(
		collection: &Self::CollectionId,
		item: &Self::ItemId,
	) -> Option<<T as SystemConfig>::AccountId> {
		SpacePass::<T>::get(collection, item).map(|a| a.owner)
	}

	fn collection_owner(collection: &Self::CollectionId) -> Option<<T as SystemConfig>::AccountId> {
		Space::<T>::get(collection).map(|a| a.owner)
	}

	/// Returns the attribute value of `item` of `collection` corresponding to `key`.
	///
	/// When `key` is empty, we return the item metadata value.
	///
	/// By default this is `None`; no attributes are defined.
	fn attribute(
		collection: &Self::CollectionId,
		item: &Self::ItemId,
		key: &[u8],
	) -> Option<Vec<u8>> {
		if key.is_empty() {
			// We make the empty key map to the item metadata value.
			SpacePassMetadataOf::<T>::get(collection, item).map(|m| m.data.into())
		} else {
			let key = BoundedSlice::<_, _>::try_from(key).ok()?;
			Attribute::<T>::get((collection, Some(item), key)).map(sp_std::convert::Into::into)
		}
	}

	/// Returns the attribute value of `item` of `collection` corresponding to `key`.
	///
	/// When `key` is empty, we return the item metadata value.
	///
	/// By default this is `None`; no attributes are defined.
	fn collection_attribute(collection: &Self::CollectionId, key: &[u8]) -> Option<Vec<u8>> {
		if key.is_empty() {
			// We make the empty key map to the item metadata value.
			SpaceMetadataOf::<T>::get(collection).map(|m| m.data.into())
		} else {
			let key = BoundedSlice::<_, _>::try_from(key).ok()?;
			Attribute::<T>::get((collection, Option::<SpacePassId>::None, key))
				.map(sp_std::convert::Into::into)
		}
	}

	/// Returns `true` if the asset `item` of `collection` may be transferred.
	///
	/// Default implementation is that all assets are transferable.
	fn can_transfer(collection: &Self::CollectionId, item: &Self::ItemId) -> bool {
		matches!((Space::<T>::get(collection), SpacePass::<T>::get(collection, item)), (Some(cd), Some(id)) if !cd.is_frozen && !id.is_frozen)
	}
}

impl<T: Config> Create<<T as SystemConfig>::AccountId> for Pallet<T> {
	/// Create a `collection` of nonfungible assets to be owned by `who` and managed by `admin`.
	fn create_collection(
		collection: &Self::CollectionId,
		who: &T::AccountId,
		admin: &T::AccountId,
	) -> DispatchResult {
		let admins = BTreeSet::from([admin.clone()]);
		Self::do_create_space(*collection, who.clone(), admins, Default::default())
	}
}

impl<T: Config> Destroy<<T as SystemConfig>::AccountId> for Pallet<T> {
	type DestroyWitness = DestroyWitness;

	fn get_destroy_witness(collection: &Self::CollectionId) -> Option<DestroyWitness> {
		Space::<T>::get(collection).map(|a| a.destroy_witness())
	}

	fn destroy(
		collection: Self::CollectionId,
		witness: Self::DestroyWitness,
		maybe_check_owner: Option<T::AccountId>,
	) -> Result<Self::DestroyWitness, DispatchError> {
		Self::do_destroy_space(collection, witness, maybe_check_owner)
	}
}

impl<T: Config> Mutate<<T as SystemConfig>::AccountId> for Pallet<T> {
	fn mint_into(
		collection: &Self::CollectionId,
		item: &Self::ItemId,
		who: &T::AccountId,
	) -> DispatchResult {
		Self::do_mint_space_pass(*collection, *item, who.clone(), |_| Ok(()))
	}

	fn burn(
		collection: &Self::CollectionId,
		item: &Self::ItemId,
		maybe_check_owner: Option<&T::AccountId>,
	) -> DispatchResult {
		Self::do_burn_space_pass(*collection, *item, |_, d| {
			if let Some(check_owner) = maybe_check_owner {
				if &d.owner != check_owner {
					return Err(Error::<T>::NoPermission.into())
				}
			}
			Ok(())
		})
	}
}

impl<T: Config> Transfer<T::AccountId> for Pallet<T> {
	fn transfer(
		collection: &Self::CollectionId,
		item: &Self::ItemId,
		destination: &T::AccountId,
	) -> DispatchResult {
		Self::do_transfer(*collection, *item, destination.clone(), |_, _| Ok(()))
	}
}

impl<T: Config> InspectEnumerable<T::AccountId> for Pallet<T> {
	/// Returns an iterator of the asset collectiones in existence.
	///
	/// NOTE: iterating this list invokes a storage read per item.
	fn collections() -> Box<dyn Iterator<Item = Self::CollectionId>> {
		Box::new(SpaceMetadataOf::<T>::iter_keys())
	}

	/// Returns an iterator of the items of an asset `collection` in existence.
	///
	/// NOTE: iterating this list invokes a storage read per item.
	fn items(collection: &Self::CollectionId) -> Box<dyn Iterator<Item = Self::ItemId>> {
		Box::new(SpacePassMetadataOf::<T>::iter_key_prefix(collection))
	}

	/// Returns an iterator of the asset items of all collectiones owned by `who`.
	///
	/// NOTE: iterating this list invokes a storage read per item.
	fn owned(who: &T::AccountId) -> Box<dyn Iterator<Item = (Self::CollectionId, Self::ItemId)>> {
		Box::new(Account::<T>::iter_key_prefix((who,)))
	}

	/// Returns an iterator of the asset items of `collection` owned by `who`.
	///
	/// NOTE: iterating this list invokes a storage read per item.
	fn owned_in_collection(
		collection: &Self::CollectionId,
		who: &T::AccountId,
	) -> Box<dyn Iterator<Item = Self::ItemId>> {
		Box::new(Account::<T>::iter_key_prefix((who, collection)))
	}
}
