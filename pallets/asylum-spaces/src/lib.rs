#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(test)]
pub mod mock;

#[cfg(test)]
mod tests;

mod functions;
mod impl_nonfungibles;
mod types;

use asylum_traits::primitives::{BlueprintId, SpaceId, SpacePassId};
use codec::{Decode, Encode};
use frame_support::traits::{
	tokens::{fungibles::Inspect as FungibleInspect, nonfungibles::Inspect as NFTInspect},
	Currency, ExistenceRequirement,
};
use frame_system::Config as SystemConfig;
use sp_runtime::{
	traits::{Saturating, StaticLookup, Zero},
	ArithmeticError, RuntimeDebug,
};
use sp_std::{collections::btree_set::BTreeSet, prelude::*};

pub use pallet::*;
pub use types::*;

#[frame_support::pallet]
pub mod pallet {
	use super::*;
	use frame_support::{pallet_prelude::*, BoundedBTreeSet};
	use frame_system::pallet_prelude::*;

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	#[pallet::without_storage_info]
	pub struct Pallet<T>(_);

	pub type BalanceOf<T> =
		<<T as Config>::Currency as Currency<<T as frame_system::Config>::AccountId>>::Balance;
	pub type AssetIdOf<T> = <T as Config>::AssetId;

	pub type BoundedDataOf<T> = BoundedVec<u8, <T as Config>::DataLimit>;
	pub type BoundedKeyOf<T> = BoundedVec<u8, <T as Config>::KeyLimit>;
	pub type BoundedValueOf<T> = BoundedVec<u8, <T as Config>::ValueLimit>;
	pub type BoundedStringOf<T> = BoundedVec<u8, <T as Config>::StringLimit>;

	pub type BoundedBlueprints<T> = BoundedBTreeSet<BlueprintId, <T as Config>::MaxBlueprints>;
	pub type BoundedAssets<T> = BoundedBTreeSet<AssetIdOf<T>, <T as Config>::MaxAssets>;
	pub type BoundedTeamMembers<T> =
		BoundedBTreeSet<<T as frame_system::Config>::AccountId, <T as Config>::MaxTeamMembers>;

	#[pallet::config]
	/// The module configuration trait.
	pub trait Config: frame_system::Config {
		/// The overarching event type.
		type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;

		/// Inspect pallet uniques to check if supported blueprints really exist
		type Uniques: NFTInspect<Self::AccountId, CollectionId = BlueprintId>;

		/// Additional data to be stored with an account's asset balance.
		type AssetId: Parameter + Member + MaybeSerializeDeserialize + Ord + MaxEncodedLen + Copy;

		/// Inspect pallet assets to check if space's assets really exist
		type Assets: FungibleInspect<Self::AccountId, AssetId = Self::AssetId>;

		type Currency: Currency<Self::AccountId>;

		/// The maximum length of data stored on-chain.
		#[pallet::constant]
		type DataLimit: Get<u32>;

		/// The maximum length of data stored on-chain.
		#[pallet::constant]
		type StringLimit: Get<u32>;

		/// The maximum length of an attribute key.
		#[pallet::constant]
		type KeyLimit: Get<u32>;

		/// The maximum length of an attribute value.
		#[pallet::constant]
		type ValueLimit: Get<u32>;

		#[pallet::constant]
		type MaxTeamMembers: Get<u32>;

		#[pallet::constant]
		type MaxBlueprints: Get<u32>;

		#[pallet::constant]
		type MaxAssets: Get<u32>;
	}

	#[pallet::storage]
	/// Details of a space.
	#[pallet::getter(fn spaces)]
	pub(super) type Space<T: Config> = StorageMap<_, Blake2_128Concat, SpaceId, SpaceDetailsFor<T>>;

	#[pallet::storage]
	/// The assets held by any given account; set out this way so that assets owned by a single
	/// account can be enumerated.
	pub(super) type Account<T: Config> = StorageNMap<
		_,
		(
			NMapKey<Blake2_128Concat, T::AccountId>, // owner
			NMapKey<Blake2_128Concat, SpaceId>,
			NMapKey<Blake2_128Concat, SpacePassId>,
		),
		(),
		OptionQuery,
	>;

	#[pallet::storage]
	/// The spaces owned by any given account; set out this way so that spaces owned by a single
	/// account can be enumerated.
	pub(super) type SpaceAccount<T: Config> = StorageDoubleMap<
		_,
		Blake2_128Concat,
		T::AccountId,
		Blake2_128Concat,
		SpaceId,
		(),
		OptionQuery,
	>;

	#[pallet::storage]
	/// The space passes in existence and their ownership details.
	pub(super) type SpacePass<T: Config> = StorageDoubleMap<
		_,
		Blake2_128Concat,
		SpaceId,
		Blake2_128Concat,
		SpacePassId,
		SpacePassDetailsFor<T>,
		OptionQuery,
	>;

	#[pallet::storage]
	/// Metadata of a space.
	pub(super) type SpaceMetadataOf<T: Config> = StorageMap<
		_,
		Blake2_128Concat,
		SpaceId,
		SpaceMetadata<BoundedDataOf<T>, BoundedStringOf<T>>,
		OptionQuery,
	>;

	#[pallet::storage]
	/// Metadata of a space pass.
	pub(super) type SpacePassMetadataOf<T: Config> = StorageDoubleMap<
		_,
		Blake2_128Concat,
		SpaceId,
		Blake2_128Concat,
		SpacePassId,
		SpacePassMetadata<BoundedDataOf<T>>,
		OptionQuery,
	>;

	#[pallet::storage]
	/// Metadata of a space or space pass.
	pub(super) type Attribute<T: Config> = StorageNMap<
		_,
		(
			NMapKey<Blake2_128Concat, SpaceId>,
			NMapKey<Blake2_128Concat, Option<SpacePassId>>,
			NMapKey<Blake2_128Concat, BoundedKeyOf<T>>,
		),
		BoundedValueOf<T>,
		OptionQuery,
	>;

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		SpaceCreated {
			space_id: SpaceId,
			owner: T::AccountId,
			admins: BTreeSet<T::AccountId>,
		},
		SpaceDestroyed {
			space_id: SpaceId,
		},
		SpacePassIssued {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			owner: T::AccountId,
		},
		SpacePassTransferred {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			from: T::AccountId,
			to: T::AccountId,
		},
		SpacePassBurned {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			owner: T::AccountId,
		},
		SpacePassFrozen {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
		},
		SpacePassThawed {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
		},
		SpaceFrozen {
			space_id: SpaceId,
		},
		SpaceThawed {
			space_id: SpaceId,
		},
		OwnerChanged {
			space_id: SpaceId,
			new_owner: T::AccountId,
		},
		TeamChanged {
			space_id: SpaceId,
			issuers: BTreeSet<T::AccountId>,
			admins: BTreeSet<T::AccountId>,
			freezers: BTreeSet<T::AccountId>,
		},
		ApprovedTransfer {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			owner: T::AccountId,
			delegate: T::AccountId,
		},
		ApprovalCancelled {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			owner: T::AccountId,
			delegate: T::AccountId,
		},
		SpaceMetadataSet {
			space_id: SpaceId,
			data: BoundedDataOf<T>,
			title: BoundedStringOf<T>,
			genre: BoundedStringOf<T>,
		},
		SpaceMetadataCleared {
			space_id: SpaceId,
		},
		SpacePassMetadataSet {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
			data: BoundedDataOf<T>,
		},
		SpacePassMetadataCleared {
			space_id: SpaceId,
			space_pass_id: SpacePassId,
		},
		AttributeSet {
			space_id: SpaceId,
			maybe_space_pass: Option<SpacePassId>,
			key: BoundedKeyOf<T>,
			value: BoundedValueOf<T>,
		},
		AttributeCleared {
			space_id: SpaceId,
			maybe_space_pass: Option<SpacePassId>,
			key: BoundedKeyOf<T>,
		},
		SpaceAddBlueprintSupport {
			space_id: SpaceId,
			blueprint_id: BlueprintId,
		},
		SpaceRemoveBlueprintSupport {
			space_id: SpaceId,
			blueprint_id: BlueprintId,
		},
		SpaceAddAssetSupport {
			space_id: SpaceId,
			asset_id: T::AssetId,
		},
		SpaceRemoveAssetSupport {
			space_id: SpaceId,
			asset_id: T::AssetId,
		},
		SetUnprivilegedMint {
			space_id: SpaceId,
			allow: bool,
		},
		SetPrice {
			space_id: SpaceId,
			price: BalanceOf<T>,
		},
	}

	#[pallet::error]
	pub enum Error<T> {
		/// The signing account has no permission to do the operation.
		NoPermission,
		/// The given asset ID is unknown.
		Unknown,
		/// The asset instance ID has already been used for an asset.
		AlreadyExists,
		/// The owner turned out to be different to what was expected.
		WrongOwner,
		/// Invalid witness data given.
		BadWitness,
		/// The asset ID is already taken.
		InUse,
		/// The asset instance or class is frozen.
		Frozen,
		/// The delegate turned out to be different to what was expected.
		WrongDelegate,
		/// There is no delegate approved.
		NoDelegate,
		/// No approval exists that would allow the transfer.
		Unapproved,
		/// No available id for Space or SpacePass
		NoAvailableId,
		/// Error
		Error,
	}

	impl<T: Config> Pallet<T> {
		/// Get the owner of the asset instance, if the asset exists.
		pub fn owner(class: SpaceId, instance: SpacePassId) -> Option<T::AccountId> {
			SpacePass::<T>::get(class, instance).map(|i| i.owner)
		}
	}

	#[pallet::call]
	impl<T: Config> Pallet<T> {
		/// Create new space
		///
		/// Origin must be Signed.
		///
		/// - `admins`: list of accounts managing the space
		/// - `price`: price of space pass
		/// - `allow_unprivileged_mint` - allow non-issuers to mint space passes
		///
		/// Emits `SpaceCreated`.
		#[pallet::weight(10_000)]
		pub fn create_space(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			admins: Vec<<T::Lookup as StaticLookup>::Source>,
			price: Option<BalanceOf<T>>,
		) -> DispatchResult {
			let owner = ensure_signed(origin)?;
			let admins = admins
				.into_iter()
				.map(T::Lookup::lookup)
				.collect::<Result<BTreeSet<T::AccountId>, _>>()?;

			Self::do_create_space(space_id, owner, admins, price)
		}
		/// Destroy space
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `witness`: witness
		///
		/// Emits `SpaceDestroyed`.
		#[pallet::weight(10_000)]
		pub fn destroy_space(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			witness: DestroyWitness,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;
			Self::do_destroy_space(space_id, witness, Some(check_owner))?;
			Ok(())
		}

		/// Mint space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `owner`: recipient on minted space pass
		///
		/// Emits `SpacePassIssued`.
		#[pallet::weight(10_000)]
		pub fn mint_space_pass(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			owner: <T::Lookup as StaticLookup>::Source,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			let owner = T::Lookup::lookup(owner)?;
			let space_details = Space::<T>::get(space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(
				space_details.allow_unprivileged_mint || space_details.issuers.contains(&sender),
				Error::<T>::NoPermission
			);
			if let Some(price) = space_details.price {
				T::Currency::transfer(
					&sender,
					&space_details.owner,
					price,
					ExistenceRequirement::KeepAlive,
				)?;
			}

			Self::do_mint_space_pass(space_id, space_pass_id, owner, |_| Ok(()))
		}

		/// Burn space spass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		///
		/// Emits `SpacePassBurned`.
		#[pallet::weight(10_000)]
		pub fn burn_space_pass(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			check_owner: Option<<T::Lookup as StaticLookup>::Source>,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let check_owner = check_owner.map(T::Lookup::lookup).transpose()?;

			Self::do_burn_space_pass(space_id, space_pass_id, |class_details, details| {
				let is_permitted =
					class_details.admins.contains(&origin) || details.owner == origin;
				ensure!(is_permitted, Error::<T>::NoPermission);
				ensure!(check_owner.map_or(true, |o| o == details.owner), Error::<T>::WrongOwner);
				Ok(())
			})
		}

		/// Transfer space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		/// - `dest`: transfer's destination
		///
		/// Emits `SpacePassTransfered`.
		#[pallet::weight(10_000)]
		pub fn transfer(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			dest: <T::Lookup as StaticLookup>::Source,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let dest = T::Lookup::lookup(dest)?;

			Self::do_transfer(space_id, space_pass_id, dest, |class_details, details| {
				if details.owner != origin && !class_details.admins.contains(&origin) {
					let approved = details.approved.take().map_or(false, |i| i == origin);
					ensure!(approved, Error::<T>::NoPermission);
				}
				Ok(())
			})
		}

		/// Freeze space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		///
		/// Emits `SpacePassFrozen`.
		#[pallet::weight(10_000)]
		pub fn freeze_space_pass(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			let mut details =
				SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;
			// If space pass exists then space also exists
			let space_details = Space::<T>::get(&space_id).unwrap();
			ensure!(space_details.freezers.contains(&origin), Error::<T>::NoPermission);

			details.is_frozen = true;
			SpacePass::<T>::insert(&space_id, &space_pass_id, &details);

			Self::deposit_event(Event::<T>::SpacePassFrozen { space_id, space_pass_id });
			Ok(())
		}

		/// Thaw space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		///
		/// Emits `SpacePassThawed`.
		#[pallet::weight(10_000)]
		pub fn thaw_space_pass(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			let mut details =
				SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;
			// If space pass exists then space also exists
			let space_details = Space::<T>::get(&space_id).unwrap();
			ensure!(space_details.admins.contains(&origin), Error::<T>::NoPermission);

			details.is_frozen = false;
			SpacePass::<T>::insert(&space_id, &space_pass_id, &details);

			Self::deposit_event(Event::<T>::SpacePassThawed { space_id, space_pass_id });
			Ok(())
		}

		/// Freeze space
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		///
		/// Emits `SpaceFrozen`.
		#[pallet::weight(10_000)]
		pub fn freeze_space(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(details.freezers.contains(&origin), Error::<T>::NoPermission);

				details.is_frozen = true;

				Self::deposit_event(Event::<T>::SpaceFrozen { space_id });
				Ok(())
			})
		}

		/// Thaw space
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		///
		/// Emits `SpaceThawed`.
		#[pallet::weight(10_000)]
		pub fn thaw_space(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(details.admins.contains(&origin), Error::<T>::NoPermission);

				details.is_frozen = false;

				Self::deposit_event(Event::<T>::SpaceThawed { space_id });
				Ok(())
			})
		}

		/// Transfer space ownership
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `owner`: new owner of the space
		///
		/// Emits `OwnerChanged`.
		#[pallet::weight(10_000)]
		pub fn transfer_space_ownership(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			owner: <T::Lookup as StaticLookup>::Source,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let owner = T::Lookup::lookup(owner)?;

			Space::<T>::try_mutate(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);

				if details.owner == owner {
					return Ok(())
				}

				SpaceAccount::<T>::remove(&details.owner, &space_id);
				SpaceAccount::<T>::insert(&owner, &space_id, ());
				details.owner = owner.clone();

				Self::deposit_event(Event::OwnerChanged { space_id, new_owner: owner });
				Ok(())
			})
		}

		/// Set space's team
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `issuers`: accounts, which can issue new space passes
		/// - `admins`: accounts, which can control space params
		/// - `freezers`: accounts, which can freeze space
		///
		/// Emits `TeamChanged`.
		#[pallet::weight(10_000)]
		pub fn set_space_team(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			issuers: BoundedVec<<T::Lookup as StaticLookup>::Source, T::MaxTeamMembers>,
			admins: BoundedVec<<T::Lookup as StaticLookup>::Source, T::MaxTeamMembers>,
			freezers: BoundedVec<<T::Lookup as StaticLookup>::Source, T::MaxTeamMembers>,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let issuers = issuers
				.into_iter()
				.map(T::Lookup::lookup)
				.collect::<Result<BTreeSet<T::AccountId>, _>>()?;
			let admins = admins
				.into_iter()
				.map(T::Lookup::lookup)
				.collect::<Result<BTreeSet<T::AccountId>, _>>()?;
			let freezers = freezers
				.into_iter()
				.map(T::Lookup::lookup)
				.collect::<Result<BTreeSet<T::AccountId>, _>>()?;
			Space::<T>::try_mutate(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);

				// Can unwrap cause we use BoundedVec with the same size as BoundedBTreeSet
				details.issuers = issuers.clone().try_into().unwrap();
				details.admins = admins.clone().try_into().unwrap();
				details.freezers = freezers.clone().try_into().unwrap();

				Self::deposit_event(Event::TeamChanged { space_id, issuers, admins, freezers });
				Ok(())
			})
		}

		/// Approve space pass transfer
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		/// - `delegate`: delegate can transfer space pass
		///
		/// Emits `ApprovedTransfer`.
		#[pallet::weight(10_000)]
		pub fn approve_transfer(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			delegate: <T::Lookup as StaticLookup>::Source,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let delegate = T::Lookup::lookup(delegate)?;

			let space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			let mut details =
				SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;

			let permitted = space_details.admins.contains(&origin) || origin == details.owner;
			ensure!(permitted, Error::<T>::NoPermission);

			details.approved = Some(delegate);
			SpacePass::<T>::insert(&space_id, &space_pass_id, &details);

			let delegate = details.approved.expect("set as Some above; qed");
			Self::deposit_event(Event::ApprovedTransfer {
				space_id,
				space_pass_id,
				owner: details.owner,
				delegate,
			});

			Ok(())
		}

		/// Cancel approval of space pass transfer
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		/// - `maybe_check_delegate`: optional check of delegate
		///
		/// Emits `ApprovalCancelled`.
		#[pallet::weight(10_000)]
		pub fn cancel_approval(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			maybe_check_delegate: Option<<T::Lookup as StaticLookup>::Source>,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			let space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			let mut details =
				SpacePass::<T>::get(&space_id, &space_pass_id).ok_or(Error::<T>::Unknown)?;

			let permitted = space_details.admins.contains(&origin) || origin == details.owner;
			ensure!(permitted, Error::<T>::NoPermission);

			let maybe_check_delegate = maybe_check_delegate.map(T::Lookup::lookup).transpose()?;
			let old = details.approved.take().ok_or(Error::<T>::NoDelegate)?;
			if let Some(check_delegate) = maybe_check_delegate {
				ensure!(check_delegate == old, Error::<T>::WrongDelegate);
			}

			SpacePass::<T>::insert(&space_id, &space_pass_id, &details);
			Self::deposit_event(Event::ApprovalCancelled {
				space_id,
				space_pass_id,
				owner: details.owner,
				delegate: old,
			});

			Ok(())
		}

		/// Set attribute of space or space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `maybe_space_pass`: optional id of space pass
		/// - `key`: the attribute key
		/// - `value`: the attribute value
		///
		/// Emits `AttributeSet`
		#[pallet::weight(10_000)]
		pub fn set_attribute(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			maybe_space_pass: Option<SpacePassId>,
			key: BoundedKeyOf<T>,
			value: BoundedValueOf<T>,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let mut space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(check_owner == space_details.owner, Error::<T>::NoPermission);

			let attribute = Attribute::<T>::get((space_id, maybe_space_pass, &key));
			if attribute.is_none() {
				space_details.attributes.saturating_inc();
			}

			Attribute::<T>::insert((&space_id, maybe_space_pass, &key), &value);
			Space::<T>::insert(space_id, &space_details);
			Self::deposit_event(Event::AttributeSet { space_id, maybe_space_pass, key, value });
			Ok(())
		}

		/// Clear attribute of space or space pass
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `maybe_space_pass`: optional id of space pass
		/// - `key`: the attribute key
		///
		/// Emits `AttributeCleared`
		#[pallet::weight(10_000)]
		pub fn clear_attribute(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			maybe_space_pass: Option<SpacePassId>,
			key: BoundedKeyOf<T>,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let mut space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(check_owner == space_details.owner, Error::<T>::NoPermission);

			if Attribute::<T>::take((space_id, maybe_space_pass, &key)).is_some() {
				space_details.attributes.saturating_dec();
				Space::<T>::insert(space_id, &space_details);
				Self::deposit_event(Event::AttributeCleared { space_id, maybe_space_pass, key });
			}
			Ok(())
		}

		/// Set space pass metadata
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		/// - `data`: space pass metadata
		///
		/// Emits `SpacePassMetadataSet`.
		#[pallet::weight(10_000)]
		pub fn set_space_pass_metadata(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
			data: BoundedDataOf<T>,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let mut space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;

			ensure!(check_owner == space_details.owner, Error::<T>::NoPermission);

			SpacePassMetadataOf::<T>::try_mutate_exists(space_id, space_pass_id, |metadata| {
				if metadata.is_none() {
					space_details.instance_metadatas.saturating_inc();
				}

				*metadata = Some(SpacePassMetadata { data: data.clone() });

				Space::<T>::insert(&space_id, &space_details);
				Self::deposit_event(Event::SpacePassMetadataSet { space_id, space_pass_id, data });
				Ok(())
			})
		}

		/// Clear space pass metadata
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `space_pass_id`: id of the space pass
		///
		/// Emits `SpacePassMetadataCleared`.
		#[pallet::weight(10_000)]
		pub fn clear_space_pass_metadata(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] space_pass_id: SpacePassId,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let mut space_details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(check_owner == space_details.owner, Error::<T>::NoPermission);

			SpacePassMetadataOf::<T>::try_mutate_exists(space_id, space_pass_id, |metadata| {
				if metadata.is_some() {
					space_details.instance_metadatas.saturating_dec();
				}
				metadata.take();
				Space::<T>::insert(&space_id, &space_details);
				Self::deposit_event(Event::SpacePassMetadataCleared { space_id, space_pass_id });
				Ok(())
			})
		}

		/// Set space metadata
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `data`: space pass metadata
		/// - `title`: space title
		/// - `genre`: space genre
		///
		/// Emits `SpaceMetadataSet`.
		#[pallet::weight(10_000)]
		pub fn set_space_metadata(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			data: BoundedDataOf<T>,
			title: BoundedStringOf<T>,
			genre: BoundedStringOf<T>,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(check_owner == details.owner, Error::<T>::NoPermission);

			SpaceMetadataOf::<T>::try_mutate_exists(space_id, |metadata| {
				Space::<T>::insert(&space_id, details);

				*metadata = Some(SpaceMetadata {
					data: data.clone(),
					title: title.clone(),
					genre: genre.clone(),
				});

				Self::deposit_event(Event::SpaceMetadataSet { space_id, data, title, genre });
				Ok(())
			})
		}

		/// Clear space metadata
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		///
		/// Emits `SpaceMetadataCleared`.
		#[pallet::weight(10_000)]
		pub fn clear_space_metadata(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
		) -> DispatchResult {
			let check_owner = ensure_signed(origin)?;

			let details = Space::<T>::get(&space_id).ok_or(Error::<T>::Unknown)?;
			ensure!(check_owner == details.owner, Error::<T>::NoPermission);

			SpaceMetadataOf::<T>::try_mutate_exists(space_id, |metadata| {
				metadata.take();
				Self::deposit_event(Event::SpaceMetadataCleared { space_id });
				Ok(())
			})
		}

		/// Set allow unpriviledged mint
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `allow`: allow or forbid unpriviledged mint
		///
		/// Emits `SetUnprivilegedMint`.
		#[pallet::weight(10_000)]
		pub fn set_allow_unpriviledged_mint(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			allow: bool,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);

				details.allow_unprivileged_mint = allow;
				Self::deposit_event(Event::SetUnprivilegedMint { space_id, allow });
				Ok(())
			})
		}

		/// Set space pass price
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `price`: new price of the space pass
		///
		/// Emits `SetPrice`.
		#[pallet::weight(10_000)]
		pub fn set_price(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			price: BalanceOf<T>,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;
			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(details.admins.contains(&origin), Error::<T>::NoPermission);

				details.price = Some(price);
				Self::deposit_event(Event::SetPrice { space_id, price });
				Ok(())
			})
		}

		/// Mark that space supports blueprint
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `blueprint_id`: id of the blueprint
		///
		/// Emits `SpaceAddBlueprintSupport`.
		#[pallet::weight(10_000)]
		pub fn add_blueprint_support(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] blueprint_id: BlueprintId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);
				ensure!(T::Uniques::collection_owner(&blueprint_id).is_some(), Error::<T>::Unknown);
				if let Some(blueprints) = &mut details.blueprints {
					blueprints.try_insert(blueprint_id).map_err(|_| Error::<T>::Error)?;
				} else {
					let mut blueprints = BoundedBTreeSet::new();
					blueprints.try_insert(blueprint_id).map_err(|_| Error::<T>::Error)?;
					details.blueprints = Some(blueprints);
				}

				Self::deposit_event(Event::SpaceAddBlueprintSupport { space_id, blueprint_id });
				Ok(())
			})
		}

		/// Mark that space doesn't support blueprint
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `blueprint_id`: id of the blueprint
		///
		/// Emits `SpaceRemoveBlueprintSupport`.
		#[pallet::weight(10_000)]
		pub fn remove_blueprint_support(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			#[pallet::compact] blueprint_id: BlueprintId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);
				if let Some(blueprints) = &mut details.blueprints {
					blueprints.remove(&blueprint_id);
				}

				Self::deposit_event(Event::SpaceRemoveBlueprintSupport { space_id, blueprint_id });
				Ok(())
			})
		}

		/// Mark that space support asset
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `asset_id`: id of the asset
		///
		/// Emits `SpaceAddAssetSupport`.
		#[pallet::weight(10_000)]
		pub fn add_asset_support(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			asset_id: T::AssetId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);
				ensure!(!T::Assets::total_issuance(asset_id).is_zero(), Error::<T>::Unknown);
				if let Some(assets) = &mut details.assets {
					assets.try_insert(asset_id).map_err(|_| Error::<T>::Error)?;
				} else {
					let mut assets = BoundedBTreeSet::new();
					assets.try_insert(asset_id).map_err(|_| Error::<T>::Error)?;
					details.assets = Some(assets);
				}

				Self::deposit_event(Event::SpaceAddAssetSupport { space_id, asset_id });
				Ok(())
			})
		}

		/// Mark that space doesn't support asset
		///
		/// Origin must be Signed.
		///
		/// - `space_id`: id of the space
		/// - `asset_id`: id of the asset
		///
		/// Emits `SpaceRemoveAssetSupport`.
		#[pallet::weight(10_000)]
		pub fn remove_asset_support(
			origin: OriginFor<T>,
			#[pallet::compact] space_id: SpaceId,
			asset_id: T::AssetId,
		) -> DispatchResult {
			let origin = ensure_signed(origin)?;

			Space::<T>::try_mutate_exists(space_id, |maybe_details| {
				let details = maybe_details.as_mut().ok_or(Error::<T>::Unknown)?;
				ensure!(origin == details.owner, Error::<T>::NoPermission);
				if let Some(assets) = &mut details.assets {
					assets.remove(&asset_id);
				}

				Self::deposit_event(Event::SpaceRemoveAssetSupport { space_id, asset_id });
				Ok(())
			})
		}
	}
}
