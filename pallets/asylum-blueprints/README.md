# Asylum Core Pallet

A simple, secure module for dealing with Asylum `Blueprints` and `Items`.

## Overview

The Asylum module provides functionality for `Blueprints` and `Items` management, including:

* Interpretation `Tag` creation
* `Blueprint` creation
* `Blueprint` destroying
* `Blueprint` update
* `Item` minting
* `Item` transfer
* `Item` burning
* `Item` update
* Compatability with [pallet_uniques](https://paritytech.github.io/substrate/master/pallet_uniques/index.html) and [RMRK pallets](https://rmrk-team.github.io/rmrk-substrate/#/pallets/rmrk-core)

### Flow diagram

![](/docs/img/asylum-flow-diagram.png)

### Terminology

Entities:
* **Blueprint:** The extension of the classic NFT Collection. The `Temlate` has a set of supported `Interpretations`, and all items minted from this `Blueprint` support these `Interpretations` as well.
* **Interpretation:** The description of the media resource, which is used to interpret the `Blueprint` in different contexts. To describe such context, `Interpretation` must be associated with the unique set of `Tags`. This set of `Tags` defines the format of `Interpretation`'s metadata.
* **Tag:** The `Tag` is used to give an `Interpretation` a special semantic allowing `Space Client` to query specific `Interpretation` according to the context of usage. `Tag` can describe a list of fields, which forms `Interpretaion`'s metadata.
* **Item:** The NFT minted from a particular `Blueprint`. `Item` has the same `Interpretation` list, specified by `Blueprint` at the time of its minting, but can differ in the future with upgrading the `Blueprint`. The owner of `Item` might reject upgrading this `Item` according to the latest updates of `Blueprint`.

Actions:
* **Blueprint update:** The action of updating `Interpretation` list of `Blueprint`. The update is divided in two steps: 
    1. Anyone creates a proposal to update `Blueprint` interpretations.
    2. DAO votes for proposal _(right now, the step is skipped)_.
    3. `Blueprint` owner applies proposal, after that, `Blueprint` will be updated.
* **Item update:** The action of updating the `Item`'s supported interpretations to the last version of the `Item`'s blueprint. Triggered automatically after `Blueprint` update, but the `Item`'s owner should accept all changes.

## Interface

### Interpretation dispatchables
* `create_interpretation_tag`: Create new interpretation tag.

### Blueprint dispatchables
* `create_blueprint`: Create new blueprint.
* `destroy_blueprint`: Destroy blueprint.
* `update_blueprint`: Update blueprint according to the proposal and request minted Items to apply this update.

### Item dispatchables
* `mint_item_from_blueprint`: Mint new item from the blueprint, i.e. mint item with the same set of supported interpretations by the blueprint.
* `transfer_item`: Move an item from the sender account to the receiver.
* `burn_item`: Destroy an item.
* `accept_item_update`: Accept all blueprint updates up to the newest version of the blueprint.

### DAO dispatchables
* `submit_blueprint_change_proposal`: Submit proposal with blueprint updates.

## Related Modules

* [`System`](https://docs.rs/frame-system/latest/frame_system/)
* [`Support`](https://docs.rs/frame-support/latest/frame_support/)
* [`Uniques`](https://paritytech.github.io/substrate/master/pallet_uniques/index.html)
* [`RMRK`](https://rmrk-team.github.io/rmrk-substrate/#/pallets/rmrk-core)

License: MIT
