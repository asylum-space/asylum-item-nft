#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet::*;

mod functions;
mod implementation;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[frame_support::pallet]
pub mod pallet {
	use asylum_traits::{
		primitives::{BlueprintId, InterpretationId, ItemId},
		Blueprint, Change, Interpretable, Interpretation, Item, TagInfo, TagsOfInfo,
	};
	use frame_support::{
		pallet_prelude::*, traits::tokens::nonfungibles::Destroy, transactional, BoundedBTreeSet,
	};
	use frame_system::{ensure_signed, pallet_prelude::OriginFor};
	pub use pallet_rmrk_core::{BoundedCollectionSymbolOf, KeyLimitOf, StringLimitOf};
	use rmrk_traits::*;

	pub type BoundedTagOf<T> = BoundedVec<u8, <T as Config>::TagLimit>;
	pub type BoundedTagsOf<T> = BoundedBTreeSet<BoundedTagOf<T>, <T as Config>::MaxTags>;

	pub type BoundedResourceOf<T> = BasicResource<StringLimitOf<T>>;

	pub type BoundedInterpretationOf<T> = Interpretation<BoundedResourceOf<T>, BoundedTagsOf<T>>;
	pub type BoundedInterpretationsOf<T> =
		BoundedVec<BoundedInterpretationOf<T>, <T as Config>::MaxInterpretationsOnBlueprintCreate>;

	pub type BoundedAddChangesOf<T> =
		BoundedVec<BoundedInterpretationOf<T>, <T as Config>::MaxAddChanges>;
	pub type BoundedModifyChangesOf<T> =
		BoundedVec<(InterpretationId, BoundedInterpretationOf<T>), <T as Config>::MaxModifyChanges>;
	pub type BoundedChangeOf<T> = Change<BoundedAddChangesOf<T>, BoundedModifyChangesOf<T>>;
	pub type BoundedChangesOf<T> = BoundedVec<BoundedChangeOf<T>, <T as Config>::MaxChanges>;

	#[pallet::config]
	pub trait Config:
		frame_system::Config + pallet_rmrk_core::Config + pallet_uniques::Config
	{
		type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;

		#[pallet::constant]
		type TagLimit: Get<u32>;
		type MaxChanges: Get<u32>;
		type MaxAddChanges: Get<u32>;
		type MaxModifyChanges: Get<u32>;
		type MaxTags: Get<u32>;
		type MaxInterpretationsOnBlueprintCreate: Get<u32>;
	}

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	/// Next available Interpretation ID.
	#[pallet::storage]
	pub type NextBlueprintInterpretationId<T: Config> =
		StorageMap<_, Twox64Concat, BlueprintId, InterpretationId, ValueQuery>;

	#[pallet::storage]
	/// Interpretation tag's infos
	#[pallet::getter(fn tags)]
	pub(super) type Tags<T: Config> =
		StorageMap<_, Blake2_128Concat, BoundedTagOf<T>, TagInfo<StringLimitOf<T>>, OptionQuery>;

	#[pallet::storage]
	/// Interpretations supported by Items
	#[pallet::getter(fn item_interpretation_tags)]
	pub(super) type ItemInterpretationTags<T: Config> = StorageNMap<
		_,
		(
			NMapKey<Twox64Concat, BlueprintId>,
			NMapKey<Twox64Concat, ItemId>,
			NMapKey<Twox64Concat, InterpretationId>,
		),
		TagsOfInfo<BoundedTagsOf<T>>,
		OptionQuery,
	>;

	#[pallet::storage]
	/// Interpretations supported by Items
	#[pallet::getter(fn blueprint_interpretations)]
	pub(super) type BlueprintIntepretations<T: Config> = StorageDoubleMap<
		_,
		Blake2_128Concat,
		BlueprintId,
		Blake2_128Concat,
		InterpretationId,
		BoundedInterpretationOf<T>,
		OptionQuery,
	>;

	#[pallet::genesis_config]
	pub struct GenesisConfig<T: Config> {
		pub interpretation_tags: Vec<(String, String)>,
		_marker: PhantomData<T>,
	}

	#[cfg(feature = "std")]
	impl<T: Config> GenesisConfig<T> {
		pub fn new(interpretation_tags: Vec<(String, String)>) -> Self {
			Self { interpretation_tags, _marker: Default::default() }
		}
	}

	#[cfg(feature = "std")]
	impl<T: Config> Default for GenesisConfig<T> {
		fn default() -> Self {
			Self { interpretation_tags: Default::default(), _marker: Default::default() }
		}
	}

	#[pallet::genesis_build]
	impl<T: Config> GenesisBuild<T> for GenesisConfig<T> {
		fn build(&self) {
			fn bounded<T>(string: &str) -> BoundedVec<u8, T>
			where
				T: Get<u32>,
			{
				TryInto::<BoundedVec<u8, T>>::try_into(string.as_bytes().to_vec()).unwrap()
			}

			for (tag, metadata) in &self.interpretation_tags {
				let metadata = bounded(metadata);
				let info = TagInfo { metadata, approved: true };
				Tags::<T>::insert(bounded(tag), info);
			}
		}
	}

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		InterpretationTagCreated { tag: BoundedTagOf<T> },
		InterpretationTagRemoved { tag: BoundedTagOf<T> },
		BlueprintCreated { blueprint_name: BoundedCollectionSymbolOf<T>, blueprint_id: BlueprintId },
		BlueprintUpdated { blueprint_id: BlueprintId },
		BlueprintDestroyed { blueprint_id: BlueprintId },
		ItemMinted { blueprint_id: BlueprintId, item_id: ItemId },
		ItemBurned { blueprint_id: BlueprintId, item_id: ItemId },
		ItemTransfered { blueprint_id: BlueprintId, item_id: ItemId, destination: T::AccountId },
		ItemUpdated { blueprint_id: BlueprintId, item_id: ItemId },
		ItemAttributeSet { item_id: ItemId, key: KeyLimitOf<T> },
		ItemAttributeCleared { item_id: ItemId, key: KeyLimitOf<T> },
	}

	#[pallet::error]
	pub enum Error<T> {
		TagAlreadyExists,
		TagUnknown,
		BlueprintDoesntSupportThisInterpretation,
		EmptyTags,
		ProposalNotExist,
		ProposalNotApproved,
		ProposalInappropriateBlueprint,
		NoAvailableId,
		NoPermission,
		BlueprintUnknown,
		ItemUnknown,
	}

	#[pallet::call]
	impl<T: Config> Pallet<T>
	where
		T: pallet_uniques::Config<CollectionId = BlueprintId, ItemId = ItemId>
			+ pallet_rmrk_core::Config,
	{
		/// Create new interpretation tag.
		///
		/// Origin must be Signed.
		///
		/// - `tag`: The interpretation tag to be created.
		/// - `metadata`: The link to the interpretation tag's metadata
		///
		/// Emits `InterpretationTagCreated`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn create_interpretation_tag(
			origin: OriginFor<T>,
			tag: BoundedTagOf<T>,
			metadata: StringLimitOf<T>,
		) -> DispatchResult {
			ensure_signed(origin)?;
			Self::interpretation_tag_create(&tag, metadata)?;
			Self::deposit_event(Event::InterpretationTagCreated { tag });
			Ok(())
		}

		/// Remove interpretation tag.
		///
		/// Origin must be Signed.
		///
		/// - `tag`: The interpretation tag to be created.
		///
		/// Emits `InterpretationTagRemoved`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn remove_interpretation_tag(
			origin: OriginFor<T>,
			tag: BoundedTagOf<T>,
		) -> DispatchResult {
			ensure_signed(origin)?;
			Self::interpretation_tag_remove(&tag)?;
			Self::deposit_event(Event::InterpretationTagRemoved { tag });
			Ok(())
		}

		/// Create new blueprint. In Asylum context Blueprint is extended
		/// Collection of NFTs, i.e. all Items minted from this Blueprint (in
		/// this Collection) should have the same interpretations.
		///
		/// Origin must be Signed.
		///
		/// - `blueprint_name`: The RMRK Collection's symbol.
		/// - `metadata`: The RMRK Collection's metadata.
		/// - `max`: The RMRK Collection's max.
		/// - `interpretations`: vec of Interpretations.
		///
		/// Emits `BlueprintCreated`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn create_blueprint(
			origin: OriginFor<T>,
			blueprint_name: BoundedCollectionSymbolOf<T>,
			metadata: StringLimitOf<T>,
			max: Option<u32>,
			interpretations: BoundedInterpretationsOf<T>,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			let blueprint_id = pallet_rmrk_core::Pallet::<T>::collection_create(
				sender.clone(),
				metadata,
				max,
				blueprint_name.clone(),
			)?;
			Self::blueprint_create(blueprint_id, interpretations)?;
			pallet_uniques::Pallet::<T>::do_create_collection(
				blueprint_id,
				sender.clone(),
				sender.clone(),
				T::CollectionDeposit::get(),
				false,
				pallet_uniques::Event::Created {
					collection: blueprint_id,
					creator: sender.clone(),
					owner: sender,
				},
			)?;

			Self::deposit_event(Event::BlueprintCreated { blueprint_name, blueprint_id });
			Ok(())
		}

		/// Destroy blueprint. In Asylum context Blueprint is extended Collection
		/// of NFTs.
		///
		/// Origin must be Signed and sender should be owner of the blueprint.
		///
		/// - `blueprint_id`: The blueprint to be destroyed.
		///
		/// Emits `BlueprintDestroyed`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn destroy_blueprint(
			origin: OriginFor<T>,
			#[pallet::compact] blueprint_id: BlueprintId,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			if let Some(collection_issuer) =
				pallet_uniques::Pallet::<T>::collection_owner(blueprint_id)
			{
				ensure!(collection_issuer == sender, Error::<T>::NoPermission);
			} else {
				return Err(Error::<T>::BlueprintUnknown.into())
			}
			Self::blueprint_destroy(blueprint_id)?;
			pallet_rmrk_core::Pallet::<T>::collection_burn(sender.clone(), blueprint_id)?;
			let witness = pallet_uniques::Pallet::<T>::get_destroy_witness(&blueprint_id).unwrap();
			pallet_uniques::Pallet::<T>::do_destroy_collection(
				blueprint_id,
				witness,
				sender.into(),
			)?;

			Self::deposit_event(Event::BlueprintDestroyed { blueprint_id });
			Ok(())
		}

		/// Update blueprint according to proposal. In Asylum context Blueprint is
		/// extended Collection of NFTs.
		///
		/// Origin must be Signed and sender should be owner of the blueprint.
		///
		/// - `blueprint_id`: The blueprint to be destroyed.
		/// - `proposal_id`: The blueprint update proposal id.
		///
		/// Emits `BlueprintUpdated`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn update_blueprint(
			origin: OriginFor<T>,
			#[pallet::compact] blueprint_id: BlueprintId,
			change_set: BoundedChangesOf<T>,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			if let Some(collection_issuer) =
				pallet_uniques::Pallet::<T>::collection_owner(blueprint_id)
			{
				ensure!(collection_issuer == sender, Error::<T>::NoPermission);
			} else {
				return Err(Error::<T>::BlueprintUnknown.into())
			}
			Self::blueprint_update(sender, blueprint_id, change_set)?;
			Self::deposit_event(Event::BlueprintUpdated { blueprint_id });
			Ok(())
		}

		/// Mint new item from 'blueprint_id', i.e. mint Item(NFT) with
		/// the same set of supported interpretations as 'blueprint_id'
		/// has.
		///
		/// Origin must be Signed and sender must be Issuer of Blueprint.
		///
		/// - `owner`: The owner of the minted item.
		/// - `blueprint_id`: The blueprint name or id.
		/// - `metadata`: The link to the item description stored somewhere(for example ipfs).
		///
		/// Emits `ItemMinted`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn mint_item(
			origin: OriginFor<T>,
			owner: T::AccountId,
			#[pallet::compact] blueprint_id: BlueprintId,
			metadata: StringLimitOf<T>,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			if let Some(collection_issuer) =
				pallet_uniques::Pallet::<T>::collection_owner(blueprint_id)
			{
				ensure!(collection_issuer == sender, Error::<T>::NoPermission);
			} else {
				return Err(Error::<T>::BlueprintUnknown.into())
			}
			let (_, item_id) = pallet_rmrk_core::Pallet::<T>::nft_mint(
				sender.clone(),
				owner.clone(),
				blueprint_id,
				None,
				None,
				metadata,
				true,
			)?;
			pallet_uniques::Pallet::<T>::do_mint(blueprint_id, item_id, owner, |_details| Ok(()))?;
			Self::item_mint(sender, blueprint_id, item_id)?;

			Self::deposit_event(Event::ItemMinted { blueprint_id, item_id });
			Ok(())
		}

		/// Destroy an item.
		///
		/// Origin must be Signed and the sender should be the Admin of the
		/// asset `blueprint_id`.
		///
		/// - `blueprint_id`: The blueprint name or id
		/// - `item_id`: The item to be burned
		///
		/// Emits `ItemBurned`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn burn_item(
			origin: OriginFor<T>,
			#[pallet::compact] blueprint_id: BlueprintId,
			#[pallet::compact] item_id: ItemId,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			if let Some(item_owner) = pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id) {
				ensure!(item_owner == sender, Error::<T>::NoPermission);
			} else {
				return Err(Error::<T>::ItemUnknown.into())
			}
			Self::item_burn(blueprint_id, item_id)?;
			let max_recursions = T::MaxRecursions::get();
			pallet_rmrk_core::Pallet::<T>::nft_burn(blueprint_id, item_id, max_recursions)?;
			pallet_uniques::Pallet::<T>::do_burn(blueprint_id, item_id, |_, _| Ok(()))?;
			Self::deposit_event(Event::ItemBurned { blueprint_id, item_id });
			Ok(())
		}

		/// Move an item from the sender account to another.
		///
		/// Origin must be Signed and the signing account must be owner of the item
		///
		/// Arguments:
		/// - `blueprint_id`: The blueprint of the item to be transferred.
		/// - `item_id`: The item to be transferred.
		/// - `destination`: The account to receive ownership of the asset.
		///
		/// Emits `ItemTransferred`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn transfer_item(
			origin: OriginFor<T>,
			#[pallet::compact] blueprint_id: BlueprintId,
			#[pallet::compact] item_id: ItemId,
			destination: AccountIdOrCollectionNftTuple<T::AccountId>,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			let (destination, _) = pallet_rmrk_core::Pallet::<T>::nft_send(
				sender,
				blueprint_id,
				item_id,
				destination,
			)?;
			pallet_uniques::Pallet::<T>::do_transfer(
				blueprint_id,
				item_id,
				destination.clone(),
				|_class_details, _details| Ok(()),
			)?;
			Self::deposit_event(Event::ItemTransfered { blueprint_id, item_id, destination });
			Ok(())
		}

		/// Update 'item_id' item according to newest version of
		/// 'blueprint_id' blueprint
		///
		/// Origin must be Signed and the sender must be owner of the 'item_id'
		/// item
		///
		/// Arguments:
		/// - `blueprint_id`: The blueprint of the item to be updated.
		/// - `item_id`: The item to be updated.
		///
		/// Emits `ItemUpdated`.
		#[pallet::weight(10_000)]
		#[transactional]
		pub fn approve_pending_changes(
			origin: OriginFor<T>,
			#[pallet::compact] blueprint_id: BlueprintId,
			#[pallet::compact] item_id: ItemId,
		) -> DispatchResult {
			let sender = ensure_signed(origin)?;
			if let Some(item_owner) = pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id) {
				ensure!(item_owner == sender, Error::<T>::NoPermission);
			} else {
				return Err(Error::<T>::ItemUnknown.into())
			}
			Self::item_accept_update(sender, blueprint_id, item_id)?;
			Self::deposit_event(Event::ItemUpdated { blueprint_id, item_id });
			Ok(())
		}
	}
}
