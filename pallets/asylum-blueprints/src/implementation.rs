use super::*;
use asylum_traits::{
	primitives::{BlueprintId, ItemId},
	*,
};
use frame_support::ensure;
use pallet_rmrk_core::StringLimitOf;
use rmrk_traits::{Resource, ResourceTypes};
use sp_runtime::DispatchResult;
use sp_std::vec::Vec;

impl<T: Config> Interpretable<StringLimitOf<T>, BoundedTagOf<T>> for Pallet<T>
where
	T: pallet_uniques::Config<CollectionId = BlueprintId, ItemId = ItemId>,
{
	fn interpretation_tag_create(
		tag: &BoundedTagOf<T>,
		metadata: StringLimitOf<T>,
	) -> DispatchResult {
		ensure!(!Tags::<T>::contains_key(&tag), Error::<T>::TagAlreadyExists);
		let type_info = TagInfo { metadata, approved: true };
		Tags::<T>::insert(&tag, type_info);
		Ok(())
	}

	fn interpretation_tag_remove(tag: &BoundedTagOf<T>) -> DispatchResult {
		Tags::<T>::remove(&tag);
		Ok(())
	}
}

impl<T: Config> Item<T::AccountId, StringLimitOf<T>, StringLimitOf<T>> for Pallet<T>
where
	T: pallet_uniques::Config<CollectionId = BlueprintId, ItemId = ItemId>,
{
	fn item_mint(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		item_id: ItemId,
	) -> DispatchResult {
		let item_owner = pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id).unwrap();
		let mut ordered_interpretations =
			BlueprintIntepretations::<T>::iter_prefix(blueprint_id).collect::<Vec<_>>();
		ordered_interpretations.sort_by(|x, y| x.0.partial_cmp(&y.0).unwrap());

		ordered_interpretations.into_iter().try_for_each(
			|(_, interpretation)| -> DispatchResult {
				let mut tags_info = TagsOfInfo {
					tags: Default::default(),
					pending_tags: Default::default(),
					pending_removal: false,
				};
				if item_owner == sender {
					tags_info.tags = interpretation.tags;
				} else {
					tags_info.pending_tags = interpretation.tags;
				}
				let next_interpretation_id =
					pallet_rmrk_core::Pallet::<T>::next_resource_id(blueprint_id, item_id);
				pallet_rmrk_core::Pallet::<T>::resource_add(
					sender.clone(),
					blueprint_id,
					item_id,
					ResourceTypes::Basic(interpretation.interpretation),
				)?;
				ItemInterpretationTags::<T>::insert(
					(blueprint_id, item_id, &next_interpretation_id),
					tags_info,
				);
				Ok(())
			},
		)?;
		Ok(())
	}

	fn item_burn(blueprint_id: BlueprintId, item_id: ItemId) -> DispatchResult {
		ItemInterpretationTags::<T>::remove_prefix((blueprint_id, item_id), None);
		Ok(())
	}

	fn item_accept_update(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		item_id: ItemId,
	) -> DispatchResult {
		pallet_rmrk_core::Resources::<T>::iter_key_prefix((blueprint_id, item_id)).try_for_each(
			|interpretation_id| -> DispatchResult {
				let interpretation = pallet_rmrk_core::Pallet::<T>::resources((
					blueprint_id,
					item_id,
					&interpretation_id,
				))
				.unwrap();
				ItemInterpretationTags::<T>::try_mutate_exists(
					(blueprint_id, item_id, &interpretation_id),
					|maybe_tags_info| -> DispatchResult {
						if let Some(tags_info) = maybe_tags_info {
							if tags_info.pending_removal {
								*maybe_tags_info = None;
							} else if !tags_info.pending_tags.is_empty() {
								tags_info.tags.clone_from(&tags_info.pending_tags);
								tags_info.pending_tags.clear();
							}
						}
						Ok(())
					},
				)?;
				if interpretation.pending_removal {
					pallet_rmrk_core::Pallet::<T>::accept_removal(
						sender.clone(),
						blueprint_id,
						item_id,
						interpretation_id,
					)?;
				} else if interpretation.pending {
					pallet_rmrk_core::Pallet::<T>::accept(
						sender.clone(),
						blueprint_id,
						item_id,
						interpretation_id,
					)?;
				}
				Ok(())
			},
		)
	}
}

impl<T: Config> Blueprint<T::AccountId, BoundedInterpretationsOf<T>, BoundedChangesOf<T>>
	for Pallet<T>
where
	T: pallet_uniques::Config<CollectionId = BlueprintId, ItemId = ItemId>,
{
	fn blueprint_create(
		blueprint_id: BlueprintId,
		interpretations: BoundedInterpretationsOf<T>,
	) -> DispatchResult {
		interpretations.into_iter().try_for_each(|interpretation| -> DispatchResult {
			ensure!(!interpretation.tags.is_empty(), Error::<T>::EmptyTags);
			interpretation.tags.iter().try_for_each(|tag| -> DispatchResult {
				ensure!(Tags::<T>::contains_key(tag), Error::<T>::TagUnknown);
				Ok(())
			})?;
			let interpretation_id = Self::get_next_interpretation_id(blueprint_id)?;
			BlueprintIntepretations::<T>::insert(blueprint_id, interpretation_id, interpretation);
			Ok(())
		})?;
		Ok(())
	}

	fn blueprint_update(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		change_set: BoundedChangesOf<T>,
	) -> DispatchResult {
		change_set
			.into_iter()
			.try_for_each(|change| Self::apply_changes(sender.clone(), blueprint_id, change))?;
		Ok(())
	}

	fn blueprint_destroy(blueprint_id: BlueprintId) -> DispatchResult {
		BlueprintIntepretations::<T>::remove_prefix(blueprint_id, None);
		Ok(())
	}
}
