use crate as pallet_asylum_blueprints;
use crate::{mock::*, Error};
use asylum_traits::{Change, Interpretation, TagInfo, TagsOfInfo};
use frame_support::{
	assert_noop, assert_ok,
	traits::{GenesisBuild, Get},
	BoundedBTreeSet, BoundedVec,
};
use rmrk_traits::{AccountIdOrCollectionNftTuple, BasicResource, ResourceInfo};

use sp_std::collections::btree_set::BTreeSet;

const PREFIX_2D: &str = "2D";
const TAG_WEAPON: &str = "weapon";
const TAG_DARK: &str = "dark";
const TAG_ASYLUM: &str = "asylum";

const TAGS: &[&str] = &[TAG_WEAPON, TAG_DARK, TAG_ASYLUM];

const MOCK_HASH: &str = "ipfs://hash";

type BoundedResource = BasicResource<BoundedVec<u8, UniquesStringLimit>>;
type BoundedTags = BoundedBTreeSet<BoundedVec<u8, TagLimit>, MaxTags>;

fn bounded<T>(string: &str) -> BoundedVec<u8, T>
where
	T: Get<u32>,
{
	TryInto::<BoundedVec<u8, T>>::try_into(string.as_bytes().to_vec()).unwrap()
}

fn interpretation(metadata: &str, tags: &[&str]) -> Interpretation<BoundedResource, BoundedTags> {
	let metadata = bounded(metadata);
	let resource = BasicResource {
		src: Some(metadata.clone()),
		metadata: Some(metadata),
		license: None,
		thumb: None,
	};
	Interpretation { tags: tags_set(tags), interpretation: resource }
}

fn create_tags() {
	for tag in TAGS {
		let name = bounded(tag);
		let metadata = bounded(MOCK_HASH);
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			name,
			metadata
		));
	}
}

fn tags_set<T>(tags: &[&str]) -> BoundedBTreeSet<BoundedVec<u8, T>, MaxTags>
where
	T: Get<u32>,
{
	let mut set = BTreeSet::new();
	for tag in tags {
		set.insert(bounded(tag));
	}
	set.try_into().unwrap()
}

fn create_blueprint() {
	create_tags();
	assert_ok!(AsylumBlueprints::create_blueprint(
		Origin::signed(ALICE),
		bounded("MyBlueprint"),
		bounded(MOCK_HASH),
		None,
		vec![
			interpretation(MOCK_HASH, &[TAG_WEAPON, TAG_DARK]),
			interpretation(MOCK_HASH, &[TAG_WEAPON, TAG_DARK])
		]
		.try_into()
		.unwrap()
	));
}

fn mint_item() {
	assert_ok!(AsylumBlueprints::mint_item(Origin::signed(ALICE), ALICE, 0, bounded(MOCK_HASH)));
}

#[test]
fn should_build_genesis() {
	ExtBuilder::default().build().execute_with(|| {
		let metadata = bounded(MOCK_HASH);
		let config = pallet_asylum_blueprints::GenesisConfig::<Test>::default();
		assert!(config.interpretation_tags.is_empty());
		let config = pallet_asylum_blueprints::GenesisConfig::<Test>::new(vec![
			(TAG_WEAPON.to_string(), MOCK_HASH.to_string()),
			(TAG_DARK.to_string(), MOCK_HASH.to_string()),
		]);
		<pallet_asylum_blueprints::GenesisConfig<Test> as GenesisBuild<Test>>::build(&config);
		assert_eq!(
			AsylumBlueprints::tags(bounded(TAG_WEAPON)).unwrap(),
			TagInfo { metadata: metadata.clone(), approved: true }
		);
		assert_eq!(
			AsylumBlueprints::tags(bounded(TAG_DARK)).unwrap(),
			TagInfo { metadata, approved: true }
		);
	});
}

#[test]
fn should_create_interpretation_tag() {
	ExtBuilder::default().build().execute_with(|| {
		let name = bounded(PREFIX_2D);
		let metadata = bounded(MOCK_HASH);
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			name.clone(),
			metadata.clone()
		));
		assert_noop!(
			AsylumBlueprints::create_interpretation_tag(
				Origin::signed(ALICE),
				name.clone(),
				metadata.clone()
			),
			Error::<Test>::TagAlreadyExists
		);
		assert_eq!(AsylumBlueprints::tags(name).unwrap(), TagInfo { metadata, approved: true });
	});
}

#[test]
fn should_remove_interpretation_tag() {
	ExtBuilder::default().build().execute_with(|| {
		let name = bounded(PREFIX_2D);
		let metadata = bounded(MOCK_HASH);
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			name.clone(),
			metadata.clone()
		));
		assert_eq!(
			AsylumBlueprints::tags(name.clone()).unwrap(),
			TagInfo { metadata, approved: true }
		);
		assert_ok!(AsylumBlueprints::remove_interpretation_tag(
			Origin::signed(ALICE),
			name.clone()
		));
		assert_eq!(AsylumBlueprints::tags(name), None);
	});
}

#[test]
fn should_create_blueprint() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		for interpretation_id in 0..2 {
			assert_eq!(
				AsylumBlueprints::blueprint_interpretations(0, interpretation_id),
				Some(interpretation(MOCK_HASH, &[TAG_WEAPON, TAG_DARK]))
			);
		}
		assert_eq!(Uniques::collection_owner(0), Some(ALICE));

		assert_noop!(
			AsylumBlueprints::create_blueprint(
				Origin::signed(ALICE),
				bounded("MyBlueprint"),
				bounded(MOCK_HASH),
				None,
				vec![interpretation(MOCK_HASH, &[])].try_into().unwrap()
			),
			Error::<Test>::EmptyTags
		);
		assert_noop!(
			AsylumBlueprints::create_blueprint(
				Origin::signed(ALICE),
				bounded("MyBlueprint"),
				bounded(MOCK_HASH),
				None,
				vec![interpretation(MOCK_HASH, &["SomeTag"])].try_into().unwrap()
			),
			Error::<Test>::TagUnknown
		);
		pallet_asylum_blueprints::NextBlueprintInterpretationId::<Test>::insert(1, u32::MAX);
		assert_noop!(
			AsylumBlueprints::create_blueprint(
				Origin::signed(ALICE),
				bounded("MyBlueprint"),
				bounded(MOCK_HASH),
				None,
				vec![
					interpretation(MOCK_HASH, &[TAG_WEAPON, TAG_DARK]),
					interpretation(MOCK_HASH, &[TAG_WEAPON, TAG_DARK])
				]
				.try_into()
				.unwrap()
			),
			Error::<Test>::NoAvailableId
		);
	});
}

#[test]
fn should_destroy_blueprint() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		assert_noop!(
			AsylumBlueprints::destroy_blueprint(Origin::signed(BOB), 0),
			Error::<Test>::NoPermission
		);
		assert_ok!(AsylumBlueprints::mint_item(
			Origin::signed(ALICE),
			ALICE,
			0,
			bounded(MOCK_HASH)
		));
		assert_noop!(
			AsylumBlueprints::destroy_blueprint(Origin::signed(ALICE), 0),
			pallet_rmrk_core::Error::<Test>::CollectionNotEmpty
		);
		assert_ok!(AsylumBlueprints::burn_item(Origin::signed(ALICE), 0, 0));
		assert_ok!(AsylumBlueprints::destroy_blueprint(Origin::signed(ALICE), 0));
		assert_noop!(
			AsylumBlueprints::destroy_blueprint(Origin::signed(ALICE), 0),
			Error::<Test>::BlueprintUnknown
		);
	});
}

#[test]
fn should_mint_item() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		assert_ok!(AsylumBlueprints::mint_item(
			Origin::signed(ALICE),
			ALICE,
			0,
			bounded(MOCK_HASH)
		));
		assert_noop!(
			AsylumBlueprints::mint_item(Origin::signed(BOB), BOB, 0, bounded(MOCK_HASH)),
			Error::<Test>::NoPermission
		);
		assert_ok!(AsylumBlueprints::mint_item(Origin::signed(ALICE), BOB, 0, bounded(MOCK_HASH)));
		assert_noop!(
			AsylumBlueprints::approve_pending_changes(Origin::signed(ALICE), 0, 1),
			Error::<Test>::NoPermission
		);
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 1));

		let tags = &[TAG_WEAPON, TAG_DARK];

		for interpretation_id in 0..2 {
			assert_eq!(
				RmrkCore::resources((0, 0, interpretation_id)),
				Some(ResourceInfo {
					id: interpretation_id,
					pending: false,
					pending_removal: false,
					resource: rmrk_traits::ResourceTypes::Basic(
						interpretation(MOCK_HASH, tags).interpretation
					),
				})
			);
			assert_eq!(
				RmrkCore::resources((0, 1, interpretation_id)),
				Some(ResourceInfo {
					id: interpretation_id,
					pending: false,
					pending_removal: false,
					resource: rmrk_traits::ResourceTypes::Basic(
						interpretation(MOCK_HASH, tags).interpretation
					),
				})
			);
			assert_eq!(
				AsylumBlueprints::item_interpretation_tags((0, 0, interpretation_id))
					.unwrap()
					.tags,
				tags_set(&tags[..])
			);
			assert_eq!(
				AsylumBlueprints::item_interpretation_tags((0, 1, &interpretation_id))
					.unwrap()
					.tags,
				tags_set(&tags[..])
			);
		}
	});
}

#[test]
fn should_transfer_item() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		for _ in 0..3 {
			mint_item();
		}

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			0,
			AccountIdOrCollectionNftTuple::AccountId(BOB)
		));
		assert_eq!(
			RmrkCore::nfts(0, 0).unwrap().owner,
			AccountIdOrCollectionNftTuple::AccountId(BOB)
		);
		assert_eq!(Uniques::owner(0, 0), Some(BOB));

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(BOB),
			0,
			0,
			AccountIdOrCollectionNftTuple::AccountId(ALICE)
		));
		assert_eq!(
			RmrkCore::nfts(0, 0).unwrap().owner,
			AccountIdOrCollectionNftTuple::AccountId(ALICE)
		);
		assert_eq!(Uniques::owner(0, 0), Some(ALICE));

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			0,
			AccountIdOrCollectionNftTuple::AccountId(BOB)
		));

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			1,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 0)
		));
		assert_ok!(RmrkCore::accept_nft(
			Origin::signed(BOB),
			0,
			1,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 0)
		));
		assert!(RmrkCore::children((0, 0), (0, 1)).is_some());
		assert_eq!(Uniques::owner(0, 1).unwrap(), RmrkCore::nft_to_account_id(0, 0),);
		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			2,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 1)
		));
		assert_ok!(RmrkCore::accept_nft(
			Origin::signed(BOB),
			0,
			2,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 1)
		));
		assert!(RmrkCore::children((0, 1), (0, 2)).is_some());
		assert_eq!(Uniques::owner(0, 2).unwrap(), RmrkCore::nft_to_account_id(0, 1));
		assert_eq!(Uniques::owner(0, 2).unwrap(), RmrkCore::nft_to_account_id(0, 1)); // RMRK virtual adress

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(BOB),
			0,
			1,
			AccountIdOrCollectionNftTuple::AccountId(ALICE)
		));
		assert_eq!(Uniques::owner(0, 2).unwrap(), RmrkCore::nft_to_account_id(0, 1));
		assert_eq!(
			RmrkCore::nfts(0, 1).unwrap().owner,
			AccountIdOrCollectionNftTuple::AccountId(ALICE)
		);
		assert_eq!(Uniques::owner(0, 1), Some(ALICE));
	});
}

#[test]
fn should_fail_transfer_item() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		for _ in 0..2 {
			mint_item();
		}

		assert_noop!(
			AsylumBlueprints::transfer_item(
				Origin::signed(ALICE),
				999,
				999,
				AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 1)
			),
			pallet_rmrk_core::Error::<Test>::NoAvailableNftId
		);

		assert_noop!(
			AsylumBlueprints::transfer_item(
				Origin::signed(BOB),
				0,
				0,
				AccountIdOrCollectionNftTuple::AccountId(BOB)
			),
			pallet_rmrk_core::Error::<Test>::NoPermission
		);

		assert_noop!(
			AsylumBlueprints::transfer_item(
				Origin::signed(ALICE),
				0,
				0,
				AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 0)
			),
			pallet_rmrk_core::Error::<Test>::CannotSendToDescendentOrSelf
		);

		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			0,
			AccountIdOrCollectionNftTuple::AccountId(BOB)
		));
		assert_ok!(AsylumBlueprints::transfer_item(
			Origin::signed(ALICE),
			0,
			1,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 0)
		));
		assert_ok!(RmrkCore::accept_nft(
			Origin::signed(BOB),
			0,
			1,
			AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 0)
		));
		assert_noop!(
			AsylumBlueprints::transfer_item(
				Origin::signed(BOB),
				0,
				0,
				AccountIdOrCollectionNftTuple::CollectionAndNftTuple(0, 1)
			),
			pallet_rmrk_core::Error::<Test>::CannotSendToDescendentOrSelf
		);
	});
}

#[test]
fn should_burn_item() {
	ExtBuilder::default().build().execute_with(|| {
		assert_noop!(
			AsylumBlueprints::burn_item(Origin::signed(ALICE), 0, 0),
			Error::<Test>::ItemUnknown
		);
		create_blueprint();
		mint_item();
		assert_ok!(AsylumBlueprints::burn_item(Origin::signed(ALICE), 0, 0));
		for interpretation_id in 0..2 {
			assert_eq!(AsylumBlueprints::item_interpretation_tags((0, 0, interpretation_id)), None);
		}
		assert_eq!(RmrkCore::nfts(0, 1), None);

		assert_noop!(
			AsylumBlueprints::burn_item(Origin::signed(ALICE), 0, 0),
			Error::<Test>::ItemUnknown
		);
	});
}

#[test]
fn should_update_blueprint_and_item() {
	ExtBuilder::default().build().execute_with(|| {
		//  Setup helper data
		create_blueprint();
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			bounded("NEW"),
			bounded(MOCK_HASH),
		));
		assert_ok!(AsylumBlueprints::mint_item(
			Origin::signed(ALICE),
			ALICE,
			0,
			bounded(MOCK_HASH)
		));
		let default_tags = &[TAG_WEAPON, TAG_DARK];
		// Setup modify change: 0 should be removed 2 should be
		// added
		let modify_interpretation = Change::Modify {
			interpretations: vec![(0, interpretation("updated_metadata", default_tags))]
				.try_into()
				.unwrap(),
		};
		//	Setup add change: 3 should be added
		let new_tags = &[TAG_ASYLUM];
		let add_interpretation = Change::Add {
			interpretations: vec![interpretation("new_intepretation", new_tags)]
				.try_into()
				.unwrap(),
		};
		let change_set_1 = vec![modify_interpretation, add_interpretation];
		//  Only blueprint owner can update blueprint
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(BOB),
				0,
				change_set_1.clone().try_into().unwrap()
			),
			Error::<Test>::NoPermission
		);
		//  Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set_1.try_into().unwrap()
		));
		//  Deprecated 0 interpretation should be removed
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 0), None);
		//  New 2 interpretation should be added
		assert_eq!(
			AsylumBlueprints::blueprint_interpretations(0, 2),
			Some(interpretation("updated_metadata", default_tags))
		);
		//  New 3 interpretation should be added
		assert_eq!(
			AsylumBlueprints::blueprint_interpretations(0, 3),
			Some(interpretation("new_intepretation", new_tags))
		);
		//  Because ALICE is both items and blueprint owner item update is accepted during blueprint
		//  update, i.e.  deprecated 0 item interpretation was removed
		assert_eq!(AsylumBlueprints::item_interpretation_tags((0, 0, 0)), None);
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 2)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: false
			})
		);
		//  new 3 item interpretation was added and accepted
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 3)),
			Some(TagsOfInfo {
				tags: tags_set(new_tags),
				pending_tags: Default::default(),
				pending_removal: false
			})
		);

		//  RMRK compatibility test
		assert_eq!(RmrkCore::resources((0, 0, 0)), None);
		assert_eq!(
			RmrkCore::resources((0, 0, 2)),
			Some(ResourceInfo {
				id: 2,
				pending: false,
				pending_removal: false,
				resource: rmrk_traits::ResourceTypes::Basic(
					interpretation("updated_metadata", default_tags).interpretation
				),
			})
		);
		assert_eq!(
			RmrkCore::resources((0, 0, 3)),
			Some(ResourceInfo {
				id: 3,
				pending: false,
				pending_removal: false,
				resource: rmrk_traits::ResourceTypes::Basic(
					interpretation("new_intepretation", new_tags).interpretation
				),
			})
		);
		//	Setup remove intepretation change
		let remove_interpretation = Change::RemoveInterpretation { interpretation_id: 1 };
		let change_set_2 = vec![remove_interpretation];
		//	Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set_2.try_into().unwrap()
		));
		//  Because ALICE is both items and blueprint owner item update is accepted during blueprint
		//	update, i.e. REFIX_2D+PIXEL interpretation was removed as stated in the proposal
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 1), None);
	});
}

#[test]
fn should_update_blueprint_and_item_pending() {
	ExtBuilder::default().build().execute_with(|| {
		//  Setup helper data
		create_blueprint();
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			bounded("NEW"),
			bounded(MOCK_HASH),
		));
		assert_ok!(AsylumBlueprints::mint_item(Origin::signed(ALICE), BOB, 0, bounded(MOCK_HASH)));
		//	Accept all interpretation added during item mint process
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 0));
		let default_tags = &[TAG_WEAPON, TAG_DARK];
		// Setup modify change: 0 should be removed 2 should be
		// added
		let modify_interpretation = Change::Modify {
			interpretations: vec![(0, interpretation("updated_metadata", default_tags))]
				.try_into()
				.unwrap(),
		};
		//	Setup add change: 3 should be added
		let new_tags = &[TAG_ASYLUM];
		let add_interpretation = Change::Add {
			interpretations: vec![interpretation("new_intepretation", new_tags)]
				.try_into()
				.unwrap(),
		};
		let change_set = vec![modify_interpretation, add_interpretation];
		//  Only blueprint owner can update blueprint
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(BOB),
				0,
				change_set.clone().try_into().unwrap()
			),
			Error::<Test>::NoPermission
		);
		//  Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set.try_into().unwrap()
		));
		//  Deprecated 0 interpretation should be removed
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 0), None);
		//  New 2 interpretation should be added
		assert_eq!(
			AsylumBlueprints::blueprint_interpretations(0, 2),
			Some(interpretation("updated_metadata", default_tags))
		);
		//  New 3 interpretation should be added
		assert_eq!(
			AsylumBlueprints::blueprint_interpretations(0, 3),
			Some(interpretation("new_intepretation", new_tags))
		);
		//	Only item owner can accept item update
		assert_noop!(
			AsylumBlueprints::approve_pending_changes(Origin::signed(ALICE), 0, 0),
			Error::<Test>::NoPermission
		);
		//	Item changes are in pending state
		//	Deprecated interpretation's tags are marked for removal
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 0)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: true
			})
		);
		//	New interpretation's tags are marked as pending
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 2)),
			Some(TagsOfInfo {
				tags: Default::default(),
				pending_tags: tags_set(default_tags),
				pending_removal: false
			})
		);
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 3)),
			Some(TagsOfInfo {
				tags: Default::default(),
				pending_tags: tags_set(new_tags),
				pending_removal: false
			})
		);
		//	Owner accept item update
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 0));
		//	Deprecated 0 item interpretation was removed
		assert_eq!(AsylumBlueprints::item_interpretation_tags((0, 0, 0)), None);
		//	New 2 and 3 item interpretation was added and accepted
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 2)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: false
			})
		);
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 3)),
			Some(TagsOfInfo {
				tags: tags_set(new_tags),
				pending_tags: Default::default(),
				pending_removal: false
			})
		);
		//	RMRK compatibility test
		assert_eq!(RmrkCore::resources((0, 0, 0)), None);
		assert_eq!(
			RmrkCore::resources((0, 0, 2)),
			Some(ResourceInfo {
				id: 2,
				pending: false,
				pending_removal: false,
				resource: rmrk_traits::ResourceTypes::Basic(
					interpretation("updated_metadata", default_tags).interpretation
				),
			})
		);
		assert_eq!(
			RmrkCore::resources((0, 0, 3)),
			Some(ResourceInfo {
				id: 3,
				pending: false,
				pending_removal: false,
				resource: rmrk_traits::ResourceTypes::Basic(
					interpretation("new_intepretation", new_tags).interpretation
				),
			})
		);
		//	Setup remove interpretation change
		let remove_interpretation = Change::RemoveInterpretation { interpretation_id: 1 };
		let change_set = vec![remove_interpretation];
		//	Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set.try_into().unwrap()
		));
		//	1 interpretation tags marked for removal
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 1)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: true
			})
		);
		// Item owner accept item update
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 0));
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 1), None);
	});
}

#[test]
fn should_be_updated_sequentially_pending() {
	ExtBuilder::default().build().execute_with(|| {
		//  Setup helper data
		create_blueprint();
		assert_ok!(AsylumBlueprints::create_interpretation_tag(
			Origin::signed(ALICE),
			bounded("NEW"),
			bounded(MOCK_HASH),
		));
		assert_ok!(AsylumBlueprints::mint_item(Origin::signed(ALICE), BOB, 0, bounded(MOCK_HASH)));
		//	Accept all interpretation added during item mint process
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 0));
		// Setup modify change: 0 should be removed 2 should be
		let default_tags = &[TAG_WEAPON, TAG_DARK];
		let modify_interpretation_1 = Change::Modify {
			interpretations: vec![(0, interpretation("updated_metadata", default_tags))]
				.try_into()
				.unwrap(),
		};
		let change_set_1 = vec![modify_interpretation_1];
		//  Only blueprint owner can update blueprint
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(BOB),
				0,
				change_set_1.clone().try_into().unwrap()
			),
			Error::<Test>::NoPermission
		);

		//  Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set_1.try_into().unwrap()
		));
		//  Deprecated 0 interpretation should be removed
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 0), None);
		//	Only item owner can accept item update
		assert_noop!(
			AsylumBlueprints::approve_pending_changes(Origin::signed(ALICE), 0, 0),
			Error::<Test>::NoPermission
		);
		//	Item changes are in pending state
		//	Deprecated interpretation's tags are marked for removal
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 0)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: true
			})
		);
		//	New interpretation's tags are marked as pending
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 2)),
			Some(TagsOfInfo {
				tags: Default::default(),
				pending_tags: tags_set(default_tags),
				pending_removal: false
			})
		);

		//	Setup modify intepretation change
		let modify_interpretation_2 = Change::Modify {
			interpretations: vec![(2, interpretation("updated_2_metadata", default_tags))]
				.try_into()
				.unwrap(),
		};
		let change_set_2 = vec![modify_interpretation_2];
		//	Update blueprint
		assert_ok!(AsylumBlueprints::update_blueprint(
			Origin::signed(ALICE),
			0,
			change_set_2.try_into().unwrap()
		));
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 0), None);
		assert_eq!(AsylumBlueprints::blueprint_interpretations(0, 2), None);
		assert_eq!(
			AsylumBlueprints::blueprint_interpretations(0, 3),
			Some(interpretation("updated_2_metadata", default_tags))
		);
		//	Owner accept item update after two blueprint updates
		assert_ok!(AsylumBlueprints::approve_pending_changes(Origin::signed(BOB), 0, 0));
		//	Deprecated PREFIX_3D+PIXEL item interpretation was removed
		assert_eq!(AsylumBlueprints::item_interpretation_tags((0, 0, 0)), None);
		assert_eq!(AsylumBlueprints::item_interpretation_tags((0, 0, 2)), None);
		//	New PREFIX_3D+NEW_PIXEL item interpretation was added and accepted
		assert_eq!(
			AsylumBlueprints::item_interpretation_tags((0, 0, 3)),
			Some(TagsOfInfo {
				tags: tags_set(default_tags),
				pending_tags: Default::default(),
				pending_removal: false
			})
		);

		//	RMRK compatibility test
		assert_eq!(RmrkCore::resources((0, 0, 0)), None);
		assert_eq!(RmrkCore::resources((0, 0, 2)), None);
		assert_eq!(
			RmrkCore::resources((0, 0, 3)),
			Some(ResourceInfo {
				id: 3,
				pending: false,
				pending_removal: false,
				resource: rmrk_traits::ResourceTypes::Basic(
					interpretation("updated_2_metadata", default_tags).interpretation
				),
			})
		);
	});
}

#[test]
fn should_fail_update_blueprint() {
	ExtBuilder::default().build().execute_with(|| {
		create_blueprint();
		let remove_interpretation = Change::RemoveInterpretation { interpretation_id: 0 };
		let remove_nonexistent_interpretation =
			Change::RemoveInterpretation { interpretation_id: 42 };
		let update_removed_interpretation = Change::Modify {
			interpretations: vec![(0, interpretation("updated_metadata", &["someTag"]))]
				.try_into()
				.unwrap(),
		};
		let update_nonexistent_interpretation = Change::Modify {
			interpretations: vec![(999, interpretation("updated_metadata", &["someTag"]))]
				.try_into()
				.unwrap(),
		};
		let empty_tags = Change::Add {
			interpretations: vec![interpretation("updated_metadata", &[])].try_into().unwrap(),
		};
		let wrong_tag = Change::Add {
			interpretations: vec![interpretation("updated_metadata", &["WrongTag"])]
				.try_into()
				.unwrap(),
		};
		let change_set_1 = vec![remove_interpretation, update_removed_interpretation];
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(ALICE),
				0,
				vec![empty_tags].try_into().unwrap()
			),
			Error::<Test>::EmptyTags
		);
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(ALICE),
				0,
				vec![wrong_tag].try_into().unwrap()
			),
			Error::<Test>::TagUnknown
		);
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(ALICE),
				0,
				change_set_1.try_into().unwrap()
			),
			Error::<Test>::BlueprintDoesntSupportThisInterpretation
		);
		let change_set_2 = vec![update_nonexistent_interpretation];
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(ALICE),
				0,
				change_set_2.try_into().unwrap()
			),
			Error::<Test>::BlueprintDoesntSupportThisInterpretation
		);
		let change_set_3 = vec![remove_nonexistent_interpretation];
		assert_noop!(
			AsylumBlueprints::update_blueprint(
				Origin::signed(ALICE),
				0,
				change_set_3.try_into().unwrap()
			),
			Error::<Test>::BlueprintDoesntSupportThisInterpretation
		);
	});
}
