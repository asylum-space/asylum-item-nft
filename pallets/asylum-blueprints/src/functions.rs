use super::*;
use asylum_traits::{primitives::*, Change, TagsOfInfo};
use frame_support::{dispatch::DispatchResult, ensure};
use rmrk_traits::{Resource, ResourceTypes};

impl<T: Config> Pallet<T>
where
	T: pallet_uniques::Config<CollectionId = BlueprintId, ItemId = ItemId>,
{
	/// Get id for next intepretation for blueprint
	pub fn get_next_interpretation_id(
		blueprint_id: BlueprintId,
	) -> Result<InterpretationId, Error<T>> {
		NextBlueprintInterpretationId::<T>::try_mutate(blueprint_id, |id| {
			let current_id = *id;
			*id = id.checked_add(1).ok_or(Error::<T>::NoAvailableId)?;
			Ok(current_id)
		})
	}

	/// Apply different changes to blueprint and items
	pub fn apply_changes(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		change: BoundedChangeOf<T>,
	) -> DispatchResult {
		match change {
			Change::Add { interpretations } =>
				Self::add_interpretation(sender, blueprint_id, interpretations),
			Change::Modify { interpretations } =>
				Self::modify_interpretation(sender, blueprint_id, interpretations),
			Change::RemoveInterpretation { interpretation_id } =>
				Self::remove_interpretation(sender, blueprint_id, interpretation_id),
		}
	}

	/// Add new intepretation to blueprint and items
	pub fn add_interpretation(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		interpretations: BoundedAddChangesOf<T>,
	) -> DispatchResult {
		interpretations.into_iter().try_for_each(|interpretation| -> DispatchResult {
			Self::validate_interpretation_tags(&interpretation)?;
			let interpretation_id = Self::get_next_interpretation_id(blueprint_id)?;
			BlueprintIntepretations::<T>::insert(blueprint_id, interpretation_id, &interpretation);
			pallet_rmrk_core::Nfts::<T>::iter_key_prefix(blueprint_id).try_for_each(
				|item_id| -> DispatchResult {
					let next_interpretation_id =
						pallet_rmrk_core::Pallet::<T>::next_resource_id(blueprint_id, item_id);
					pallet_rmrk_core::Pallet::<T>::resource_add(
						sender.clone(),
						blueprint_id,
						item_id,
						ResourceTypes::Basic(interpretation.interpretation.clone()),
					)?;
					let item_owner =
						pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id).unwrap();
					//	Add tags for new interpretation
					let mut tags_info = TagsOfInfo {
						tags: Default::default(),
						pending_tags: Default::default(),
						pending_removal: false,
					};
					if item_owner == sender {
						tags_info.tags = interpretation.tags.clone();
					} else {
						tags_info.pending_tags = interpretation.tags.clone();
					}
					ItemInterpretationTags::<T>::insert(
						(blueprint_id, item_id, next_interpretation_id),
						tags_info,
					);
					Ok(())
				},
			)?;
			Ok(())
		})
	}

	/// Modify intepretation of blueprint and items
	pub fn modify_interpretation(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		interpretations: BoundedModifyChangesOf<T>,
	) -> DispatchResult {
		// Validate requested changes
		interpretations.iter().try_for_each(
			|(deprecated_id, new_interpretation)| -> DispatchResult {
				ensure!(
					BlueprintIntepretations::<T>::contains_key(blueprint_id, deprecated_id),
					Error::<T>::BlueprintDoesntSupportThisInterpretation
				);
				Self::validate_interpretation_tags(new_interpretation)?;
				Ok(())
			},
		)?;
		interpretations.into_iter().try_for_each(
			|(deprecated_id, interpretation)| -> DispatchResult {
				let interpretation_id = Self::get_next_interpretation_id(blueprint_id)?;
				BlueprintIntepretations::<T>::insert(
					blueprint_id,
					interpretation_id,
					&interpretation,
				);
				BlueprintIntepretations::<T>::remove(blueprint_id, &deprecated_id);

				pallet_rmrk_core::Nfts::<T>::iter_key_prefix(blueprint_id).try_for_each(
					|item_id| -> DispatchResult {
						//	Mark(or remove) deprecated interpretation
						pallet_rmrk_core::Pallet::<T>::resource_remove(
							sender.clone(),
							blueprint_id,
							item_id,
							deprecated_id,
						)?;
						let item_owner =
							pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id).unwrap();
						//	Mark(or remove) deprecated interpretation's tags
						if item_owner == sender {
							ItemInterpretationTags::<T>::remove((
								blueprint_id,
								item_id,
								&deprecated_id,
							));
						} else {
							ItemInterpretationTags::<T>::try_mutate(
								(blueprint_id, item_id, &deprecated_id),
								|maybe_tags_info| -> DispatchResult {
									if let Some(tags_info) = maybe_tags_info {
										tags_info.pending_removal = true;
									}
									Ok(())
								},
							)?;
						}
						let next_interpretation_id =
							pallet_rmrk_core::Pallet::<T>::next_resource_id(blueprint_id, item_id);
						//	Add new resource `instead` of deprecated one
						pallet_rmrk_core::Pallet::<T>::resource_add(
							sender.clone(),
							blueprint_id,
							item_id,
							ResourceTypes::Basic(interpretation.interpretation.clone()),
						)?;

						//	Add tags for new interpretation
						let mut tags_info = TagsOfInfo {
							tags: Default::default(),
							pending_tags: Default::default(),
							pending_removal: false,
						};
						if item_owner == sender {
							tags_info.tags = interpretation.tags.clone();
						} else {
							tags_info.pending_tags = interpretation.tags.clone();
						}
						ItemInterpretationTags::<T>::insert(
							(blueprint_id, item_id, next_interpretation_id),
							tags_info,
						);

						Ok(())
					},
				)?;
				Ok(())
			},
		)
	}

	/// Remove intepretation from blueprint and items
	pub fn remove_interpretation(
		sender: T::AccountId,
		blueprint_id: BlueprintId,
		interpretation_id: InterpretationId,
	) -> DispatchResult {
		ensure!(
			BlueprintIntepretations::<T>::contains_key(blueprint_id, &interpretation_id),
			Error::<T>::BlueprintDoesntSupportThisInterpretation
		);
		BlueprintIntepretations::<T>::remove(blueprint_id, &interpretation_id);
		pallet_rmrk_core::Nfts::<T>::iter_key_prefix(blueprint_id).try_for_each(
			|item_id| -> DispatchResult {
				pallet_rmrk_core::Pallet::<T>::resource_remove(
					sender.clone(),
					blueprint_id,
					item_id,
					interpretation_id,
				)?;
				ItemInterpretationTags::<T>::try_mutate_exists(
					(blueprint_id, item_id, &interpretation_id),
					|maybe_tags_info| -> DispatchResult {
						let item_owner =
							pallet_uniques::Pallet::<T>::owner(blueprint_id, item_id).unwrap();
						if let Some(tags_info) = maybe_tags_info {
							if item_owner == sender.clone() {
								*maybe_tags_info = None;
							} else {
								tags_info.pending_removal = true;
							}
						}
						Ok(())
					},
				)?;
				Ok(())
			},
		)
	}

	pub fn validate_interpretation_tags(
		interpretation: &BoundedInterpretationOf<T>,
	) -> DispatchResult {
		ensure!(!interpretation.tags.is_empty(), Error::<T>::EmptyTags);
		interpretation.tags.iter().try_for_each(|tag| -> DispatchResult {
			ensure!(Tags::<T>::contains_key(tag), Error::<T>::TagUnknown);
			Ok(())
		})?;
		Ok(())
	}
}
