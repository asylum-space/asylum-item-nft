//! Substrate Node Blueprint CLI library.
#![warn(missing_docs)]

fn main() -> sc_cli::Result<()> {
	node_asylum::run()
}
