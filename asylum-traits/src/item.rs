use sp_runtime::DispatchResult;

use crate::primitives::{BlueprintId, ItemId};

/// Trait for providing basic functionality of Asylum Item
pub trait Item<AccountId, BoundedName, BoundedString> {
	/// Create item with the same set of interpretations as Blueprint has
	///
	/// # Arguments
	///
	/// * `sender` - transaction sender
	/// * `blueprint_id` - blueprint id
	/// * `item_id` - id of nft minted by pallet-rmrk-core
	fn item_mint(sender: AccountId, blueprint_id: BlueprintId, item_id: ItemId) -> DispatchResult;

	/// Destroy item
	///
	/// # Arguments
	///
	/// * `blueprint_id` - blueprint id
	/// * `item_id` - id of item
	fn item_burn(blueprint_id: BlueprintId, item_id: ItemId) -> DispatchResult;

	/// Accept item's update
	///
	/// # Arguments
	///
	/// * `sender` - transaction sender
	/// * `blueprint_name_or_id` - blueprint name or blueprint id
	/// * `item_id` - id of the item to update
	fn item_accept_update(
		sender: AccountId,
		blueprint_id: BlueprintId,
		item_id: ItemId,
	) -> DispatchResult;
}

/// Trait for providing attributes support for Asylum Item
pub trait Properties<AccountId, BoundedName, BoundedString> {
	/// Set property Item(NFT)
	fn set_property(
		blueprint_id: BlueprintId,
		item_id: ItemId,
		key: BoundedString,
		value: BoundedString,
		property_owner: AccountId,
	);

	/// Clear property Item(NFT)
	fn clear_property(
		blueprint_id: BlueprintId,
		item_id: ItemId,
		key: BoundedString,
		property_owner: AccountId,
	);
}
