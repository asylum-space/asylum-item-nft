use codec::{Decode, Encode};
use frame_support::pallet_prelude::MaxEncodedLen;

use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;

use crate::primitives::*;

/// Store tag's metadata
#[derive(Encode, Decode, Eq, PartialEq, Clone, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct TagInfo<BoundedString> {
	/// ipfs hash
	pub metadata: BoundedString,
	/// approved by Senate
	pub approved: bool,
}

/// Store intepretation's tags status
#[derive(Encode, Decode, Eq, PartialEq, Clone, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct TagsOfInfo<BoundedTags> {
	/// Tags approved by item owner
	pub tags: BoundedTags,
	/// Tags to be approved by item owner
	pub pending_tags: BoundedTags,
	/// Mark all tags to be removed. Should be consistent with `pending_removal` of interpretation
	pub pending_removal: bool,
}

/// Asylum Intepretation = Asylum tags + RMRK Resource
#[derive(Encode, Decode, Eq, PartialEq, Clone, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct Interpretation<BoundedResourceInfo, BoundedTags> {
	pub tags: BoundedTags,
	pub interpretation: BoundedResourceInfo,
}

/// Possible changes of Blueprint
#[derive(Encode, Decode, Eq, PartialEq, Clone, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum Change<BoundedAddChanges, BoundedModifyChanges> {
	Add { interpretations: BoundedAddChanges },
	Modify { interpretations: BoundedModifyChanges },
	RemoveInterpretation { interpretation_id: InterpretationId },
}
