#![cfg_attr(not(feature = "std"), no_std)]

pub mod blueprint;
pub mod interpretation;
pub mod item;
pub mod types;

pub use blueprint::Blueprint;
pub use interpretation::Interpretable;
pub use item::{Item, Properties};
pub use types::*;

pub mod primitives {
	pub type ItemId = u32;
	pub type BlueprintId = u32;
	pub type InterpretationId = u32;
	pub type SpaceId = u32;
	pub type SpacePassId = u32;
}
