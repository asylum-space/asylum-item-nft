use crate::primitives::BlueprintId;

use sp_runtime::DispatchResult;

/// Trait for providing Blueprint(NFT collection) functionality for Asylum
pub trait Blueprint<AccountId, BoundedInterpretations, BoundedChanges> {
	/// Create new item's Blueprint
	///
	/// # Arguments
	///
	/// * `blueprint_id` - Collection's id created by pallet-rmrk-core
	/// * `interpretations` - vec of pairs of (InterpretationTypeName, Interpretation)
	///
	/// # Return
	///
	/// Ok(id) of newly create blueprint
	fn blueprint_create(
		blueprint_id: BlueprintId,
		interpretations: BoundedInterpretations,
	) -> DispatchResult;

	/// Update item's Blueprint according to approved proposal
	///
	/// # Arguments
	///
	/// * `sender` - transaction sender
	/// * `blueprint_id` - Blueprint's id
	fn blueprint_update(
		sender: AccountId,
		blueprint_id: BlueprintId,
		change_set: BoundedChanges,
	) -> DispatchResult;

	/// Destroy empty blueprint
	///
	/// # Arguments
	///
	/// * `blueprint_id` - Blueprint's id
	///
	/// # Return
	///
	/// Ok(id) of destroyed blueprint
	fn blueprint_destroy(blueprint_id: BlueprintId) -> DispatchResult;
}
